package Journal;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class Entry extends Journal {
        
    
    Topic t = new Topic();
    Scripture s = new Scripture();
    public void readFile() {
         try {
          File inputFile;
            inputFile = new File(savedArgs);
         DocumentBuilderFactory dbFactory 
            = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         
         NodeList nList = doc.getElementsByTagName("entry");
         
         for (int temp = 0; temp < nList.getLength(); temp++) {
           
            Node nNode = nList.item(temp);
            
            
             if (nNode.getNodeType() == Node.ELEMENT_NODE) {
            
               Element eElement = (Element) nNode;
               
               System.out.println("----");
               System.out.println("Entry");
               System.out.println("Date: " + (eElement.getAttribute("date")));
               System.out.println("Content:");
               System.out.println(eElement.getElementsByTagName("content").item(0).getTextContent());
               System.out.println("Annotations:");
               t.readFile();
               
              
             }
      
         }	
        
      } catch (Exception e) {
           e.printStackTrace();
       }
            
    }  
    
}