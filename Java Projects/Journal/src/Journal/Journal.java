package Journal;
import java.io.*;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class Journal {
    
    
    Scanner scan;
    Scanner input = new Scanner(System.in);
    String fileName;
    //File file = new File(fileName);
    Annotation a;
    public static String savedArgs;
    
    public static void main(String[] args) {
        
        Entry e = new Entry();
        Topic t = new Topic();
        Scripture s = new Scripture();
        e.readFile();
        savedArgs = args[0];
    }
    
    
    public void readFile() {
        Topic t = new Topic();
        Scripture s = new Scripture();
        try {
          File inputFile = new File(savedArgs);
         DocumentBuilderFactory dbFactory 
            = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         
         NodeList nList = doc.getElementsByTagName(t.getDisplayText());
         NodeList nList2 = doc.getElementsByTagName(s.getDisplayText());
         for (int temp = 0; temp < nList.getLength(); temp++) {
           
            Node nNode = nList.item(temp);
            Node nNode2 = nList2.item(temp);
            
             if (nNode.getNodeType() == Node.ELEMENT_NODE) {
            
               Element eElement = (Element) nNode;
               Element eElement2 = (Element) nNode2;
               System.out.println(("-") + (eElement2.getAttribute("book")) + (" ") + (eElement2.getAttribute("chapter")) + (":") + (eElement2.getAttribute("startVerse")) + (" ") + (eElement2.getAttribute("endVerse")));
               System.out.println(("-") + (eElement.getAttribute("name")));
             }
      
         }	
        
      } catch (ParserConfigurationException | SAXException | IOException e) {
       }
            
    }
}