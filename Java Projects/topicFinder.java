import java.io.FileReader;
import java.io.BufferedReader;
import java.io.File;
import java.util.Scanner;


public class Topic {
	
	public static int countWords(String str)
    {
        int count = 1;
        for (int i=0;i<=str.length()-1;i++)
        {
            if (str.charAt(i) == ' ' && str.charAt(i+1)!=' ')
            {
                count++;
            }
        }
        return count;
    }
	
	public static void main(String[] args) {
	
	
	String fileName = args[0];
	System.out.println("Opening file" + "'" + fileName + "'...");
	try {
		File myFile = new File(fileName);
		FileReader fileReader = new FileReader(myFile);
		BufferedReader reader = new BufferedReader(fileReader);
		String line = null;
		while ((line = reader.readLine()) != null) {
			
			Scanner in = new Scanner(reader);
			System.out.print(countWords(line));
			System.out.print(": ");
			System.out.println(line);
		}
		reader.close();
	}
	catch(Exception ex) {
	System.out.println("Error reading" + "'" + fileName + "'");
	}
		
	}
	
}