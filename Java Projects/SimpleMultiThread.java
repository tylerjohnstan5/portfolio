public class SimpleMultiThread {
    static Object lock = new Object();

    public static void main(String[] args) {
        Thread t1;
        t1 = new Thread(() -> {
            for (int itr = 1; itr < 101; itr = itr + 2) {
                synchronized (lock) {
                    System.out.print(" " + itr);
                    try {
                        lock.notify();
                        lock.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        });
        Thread t2;
        t2 = new Thread(() -> {
            for (int itr = 2; itr < 101; itr = itr + 2) {
                synchronized (lock) {
                    System.out.print(" " + itr);
                    try {
                        lock.notify();
                        if(itr==100)
                            break;
                        lock.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        });
        try {
            System.out.println("\n Running");
            t1.sleep(100);
            t1.start();
            t2.sleep(100);
            t2.start();
            t1.join();
            t2.join();
            System.out.println("\nAll finished");
        } catch (Exception e) {

        }
    }
}