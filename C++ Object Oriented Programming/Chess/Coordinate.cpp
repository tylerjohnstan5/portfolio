#include "Coordinate.h"
#include <cassert>

/*
 * Initializes coordinate
 */
Coordinate::Coordinate()
{
   this->setValid(true);
}

/*
 * Sets the Row
 */
void Coordinate::setRow(char value)
{
   this->row = value;
   assert(value >= 0);//cannot be negative
   if(value != '1' && value != '2' && value != '3' && value != '4' &&
      value != '5' && value != '6' && value != '7' && value != '8')
   {
      this->setValid(false);
   }
}

/*
 * Set the Col
 */
void Coordinate::setCol(char value)
{
   this->col = value;
   assert(value >= 0);//cannot be negative
   if(value != 'a' && value != 'b' && value != 'c' && value != 'd' &&
      value != 'e' && value != 'f' && value != 'g' && value != 'h')
   {
      this->setValid(false);
   }
}

/*
 * Sets that it is valid
 */
void Coordinate::setValid(bool value)
{
  this->fValid = value;
  assert(value >= 0 && value <= 1); //must be 0 or 1
}

/*
 * Gets the row
 */
char Coordinate::getRow() //gets the row
{
   return this->row;
}

/*
 * Gets the Col
 */
char Coordinate::getCol() //gets the col
{
   return this->col;
}

/*
 * Checks if the coordinate is valid
 */
bool Coordinate::isValid() //checks if a move is valid
{
   return this->fValid;
}

/**
 * Overloaded extraction operator that prints the coordinate to the screen.
 */
std::ostream & operator << (std::ostream & out, Coordinate & rhs)
{
   out << rhs.getCol() << rhs.getRow();
   return out;
}

/**
 * Overloaded insertion operator that reads the coordinate.
 */
std::istream & operator >> (std::istream & in, Coordinate & rhs)
{
   char col;
   char row;
   
   in >> col >> row;
   rhs.setCol(col);
   rhs.setRow(row);
   assert(col >= 0 && row >= 0); // cannot be negative
   return in;
}

