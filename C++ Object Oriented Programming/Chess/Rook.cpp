//
// Creates the Rook piece to be used throughout the program
//

#include "Rook.h"

Rook::Rook(bool isWhite) : Piece(isWhite)
{
   setName('R');
   setScore(5);
}
