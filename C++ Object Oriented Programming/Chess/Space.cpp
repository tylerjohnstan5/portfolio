//
// Creates the Space "piece" to be used throughout the program
//

#include "Space.h"

Space::Space() : Piece(false)
{
   setName(' ');
   setIsSpace(true);
   setScore(0);
}
