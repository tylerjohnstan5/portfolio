#include <assert.h>
#include "Piece.h"

/*
 * Sets the displayed character for each piece
 */
void Piece::setName(char name)
{
   assert(name == ' '
          || name == 'p'
          || name == 'P'
          || name == 'k'
          || name == 'K'
          || name == 'q'
          || name == 'Q'
          || name == 'r'
          || name == 'R'
          || name == 'n'
          || name == 'N'
          || name == 'b'
          || name == 'B'
   );
   this->_name = name;
   this->_moved = false;
}

/**
 * Returns the text name of the piece.
 */
char Piece::getName()
{
   return this->_name;
}

/**
 * ctor
 */
Piece::Piece(bool isWhite)
{
   this->_isWhite = isWhite;
   setIsSpace(false);
}

/*
 * Is the piece white?
 */
bool Piece::isWhite()
{
   return this->_isWhite;
}

/*
 * Is the "piece" a space?
 */
bool Piece::isSpace()
{
   return this->_isSpace;
}

/*
 * Sets the "piece" to a space
 */
void Piece::setIsSpace(bool value)
{
   this->_isSpace = value;
}

/*
 * Has the piece specified moved?
 */
bool Piece::hasMoved()
{
   return _moved;
}

/*
 * Sets that the piece has moved
 */
void Piece::setMoved()
{
   _moved = true;
}

/**
 * Sets a Piece's score
 */
void Piece::setScore(int score)
{
   _score = score;
}

int Piece::getScore()
{
   return _score;
}
