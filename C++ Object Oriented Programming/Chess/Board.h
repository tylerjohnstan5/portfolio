#ifndef PROCEDURAL_CHESS_BOARD_H
#define PROCEDURAL_CHESS_BOARD_H

#include <iostream>
#include <assert.h>
#include "Piece.h"
#include "Coordinate.h"
#include "Move.h"
#include "Space.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"
#include "Pawn.h"

class Board {

private:
   Piece * boardGrid[8][8];
   bool testMode;
   void clearScreen();
   void printHeader();
   void printRowThatStartsWithBlack(int rowNumber);
   void printRowThatStartsWithWhite(int rowNumber);
   void printRedSquare(int row, int col);
   void printWriteSquare(int row, int col);
   char formatPieceName(char name, bool isWhite);
   void handleCastle(Move & move, Piece * piece);
   void handleEnpassant(Move & move);
   void handlePromote(Move & move, Piece * piece);
public:
   void toggleTestMode();
   void printBoard();
   bool isTestMode();
   Board();
   ~Board();
   Piece * getPiece(Coordinate cords);
   void movePiece(Move move);
   void removePiece(Coordinate cords);
   void newGame();
   char getRowLabel(int row);
   int  getRowFromLabel(char row);
   char getColLabel(int col);
   int  getColFromLabel(char col);
   float getRankSpace();
   float getRankForce();

   // extraction operator
   friend std::ostream & operator << (std::ostream & out, Board & rhs);
};


#endif //PROCEDURAL_CHESS_BOARD_H
