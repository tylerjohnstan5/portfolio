//
// Creates the Bishop piece to be used throughout the program
//

#include "Bishop.h"

Bishop::Bishop(bool isWhite) : Piece(isWhite)
{
   setName('B');
   setScore(3);
}
