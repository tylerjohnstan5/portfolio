//
// Created by Nicholas Dunnaway on 1/16/16.
//

#ifndef PROCEDURAL_CHESS_QUEEN_H
#define PROCEDURAL_CHESS_QUEEN_H


#include "Piece.h"

class Queen : public Piece {

public:
    Queen(bool isWhite);
};


#endif //PROCEDURAL_CHESS_QUEEN_H
