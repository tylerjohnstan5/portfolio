#ifndef PROCEDURAL_CHESS_PIECE_H
#define PROCEDURAL_CHESS_PIECE_H

// Abstract
class Piece {

private:
   char _name;
   bool _isWhite;
   bool _isSpace;
   bool _moved;
   int _score;

protected:
   void setName(char name);
   void setScore(int score);
   void setIsSpace(bool value);

public:
   Piece(bool isWhite);
   char getName();
   int getScore();
   bool isWhite();
   bool isSpace();
   bool hasMoved();
   void setMoved();
};


#endif //PROCEDURAL_CHESS_PIECE_H
