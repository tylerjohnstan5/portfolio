//
// Creates the Knight piece to be used throughout the program
//

#include "Knight.h"

Knight::Knight(bool isWhite) : Piece(isWhite)
{
   setName('N');
   setScore(3);
}
