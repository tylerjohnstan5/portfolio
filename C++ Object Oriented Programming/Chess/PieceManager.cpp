#include "PieceManager.h"

/**
 * ctor
 */
PieceManager::PieceManager(Board & board) : board(board)
{
}

/**
 * Manages the rule enforcement for chess piece movement.
 * Main entry point for validation.
 */
void PieceManager::updateBoard(Move move, bool isWhitesTurn)
{
   Piece * pFrom = board.getPiece(move.getSrc());
   Piece * pTo = board.getPiece(move.getDest());

   // Check that from is a piece
   if(pFrom->isSpace())
   {
      throw "Piece not present in source coordinate";
   }

   // check we are moving the correct team.
   if(isWhitesTurn != pFrom->isWhite())
   {
      throw "Illegal to move your opponent's piece";
   }

   // Check that dest is space but move is a capture.
   if(pTo->isSpace() && move.isCapture() && !move.isEnpassant())
   {
      throw "Illegal move.  The piece cannot move to this location.";
   }

   // check that dest is not a space and the move is not a capture.
   if(!pTo->isSpace() && !move.isCapture())
   {
      throw "Illegal move.  The piece cannot move to this location.";
   }

   // Are we capturing the correct color?
   if(move.isCapture() && pFrom->isWhite() == pTo->isWhite() && !move.isEnpassant())
   {
      throw "Illegal move.  The piece cannot move to this location.";
   }

   // Most validation is now being handled in in sub-methods.
   if (isValidMove(move))
   {
      board.movePiece(move);
      // set last move to this move
      lastMove = new Move(move);
   }
   else
   {
      throw "Illegal move.  The piece cannot move to this location.";
   }
}

/**
 * Check if a move is valid.
 */
bool PieceManager::isValidMove(Move move)
{
   Piece * pFrom = board.getPiece(move.getSrc());

   // Check is move is out of range.
   if (!move.getSrc().isValid() && !move.getDest().isValid())
   {
      return false;
   }

   // check if move is an attempt to promote a non-pawn
   if(move.isPromote() && pFrom->getName() != 'P') return false;

   switch (pFrom->getName())
   {
      case 'P':
         return this->isValidMove((Pawn &) *pFrom, move); // Foster

      case 'K':
         return this->isValidMove((King &) *pFrom, move); // Nick

      case 'B':
         return this->isValidMove((Bishop &) *pFrom, move); // Nick

      case 'N':
         return this->isValidMove((Knight &) *pFrom, move); // Foster

      case 'Q':
         return this->isValidMove((Queen &) *pFrom, move); // Tyler

      case 'R':
         return this->isValidMove((Rook &) *pFrom, move); // Tyler

      default:
         return false; // We will never get to here.
   }
}

/**
 * Validate Pawn movement rules.
 */
bool PieceManager::isValidMove(Pawn & piece, Move move)
{
   // Be careful - rows and columns are numeric and start from top left
   // Pawns start :
   // white in row 6
   // black in row 1
   // get move coordinates as ints
   int fromRow = board.getRowFromLabel(move.getSrc().getRow());
   int toRow = board.getRowFromLabel(move.getDest().getRow());
   int fromCol = board.getColFromLabel(move.getSrc().getCol());
   int toCol = board.getColFromLabel(move.getDest().getCol());

   // set some constants to use if black of white
   int firstMoveStart =  piece.isWhite() ?  6 : 1;
   int moveDirection  =  piece.isWhite() ? -1 : 1;
   
   // handle moving forward 1 or 2 spaces - can only move straight
   if (toCol == fromCol) // moving up or down
   {
      if (abs(fromRow - toRow) == 1) // moving one space
      {
         // check the space just ahead.  If full kill move.
         return checkLocation(toRow, toCol) == SPACE;
      }
      if (abs(fromRow - toRow) == 2 && fromRow == firstMoveStart) // moving two spaces
      {
         return (checkLocation(fromRow + moveDirection, fromCol) == SPACE) &&
                (checkLocation(fromRow + moveDirection + moveDirection, fromCol) == SPACE);
      }
      return false;
   }

   // Is this a capture.
   if (move.isCapture() && !move.isEnpassant())
   {
      // There are just two possible places left for this pawn to move.
      // Check that the move is one of them.
      if ((toRow == fromRow + moveDirection)  && // correct row
         (abs(fromCol - toCol) == 1))  // one space ether way
      {
         // Is there is piece where the piece is trying to move?
         switch (checkLocation(toRow, toCol))
         {
            case SPACE:
               return false; // nobody to capture
               break; 
            case WHITE:
               return !piece.isWhite();
               break;
            case BLACK:
               return piece.isWhite();
               break;
         }
      }
   }

   if (move.isEnpassant())
   {
      // There are two possible places for this pawn to move.
      // Check that the move is one of them.
      if ((toRow == fromRow + moveDirection)  && // correct row
         (abs(fromCol - toCol) == 1))  // one space ether way
         {
            // Is there is piece where the piece is trying to move?
            if ((checkLocation(toRow, toCol)) == SPACE)
            {
               // clear to move here
               // check for the enpassant
               if (checkLocation(toRow - moveDirection, toCol) == SPACE)
               {
                  return false; // nobody to capture
               }

               // check to make sure capture piece is a pawn
               Coordinate takeMe = getCoordByOffSet(move.getSrc(), toCol - fromCol, 0);
               Piece * pTake = board.getPiece(takeMe);
               if (( pTake->isWhite() == piece.isWhite()) ||
                     tolower(pTake->getName()) != 'p')
               {
                  return false; // piece is same color
               }

               // use lastMove
               // confirm that the last move was the pawn we are taking
               // and that it just moved two spaces
               int lastSrcRow = board.getRowFromLabel(this->lastMove->getSrc().getRow());
               int lastDestRow = board.getRowFromLabel(this->lastMove->getDest().getRow());
               int lastDestCol = board.getColFromLabel(this->lastMove->getDest().getCol());
               if (lastDestCol != toCol) return false;
               return abs(lastSrcRow - lastDestRow) == 2;
            }
         }
   }
   return false;
}

/**
 * Validate King movement rules.
 */
bool PieceManager::isValidMove(King & piece, Move move)
{
   Coordinate src = move.getSrc();
   Coordinate dest = move.getDest();

   int fromRow = board.getRowFromLabel(src.getRow());
   int toRow = board.getRowFromLabel(dest.getRow());

   int rowDistance = fromRow - toRow; // How far are we moving on the Y

   // King can only move up and down one or zero is moving left to right.
   if((rowDistance < -1 || rowDistance > 1) && !move.isCastle())
   {
      return false; // King is moving too far.
   }

   int fromCol = board.getColFromLabel(src.getCol());
   int toCol = board.getColFromLabel(dest.getCol());

   int colDistance = fromCol - toCol; // How far are we moving on the X

   // King can only move left and right one or zero is moving up or down
   if((colDistance < -1 || colDistance > 1) && !move.isCastle())
   {
      return false; // King is moving too far.
   }

   // Check castle rules
   if (move.isCastle())
   {
      // Has the king moved?
      if(piece.hasMoved())
      {
         return false;
      }

      // Validate the castle move.
      Coordinate coordinate;
      if (move.isCastleK())
      {
         coordinate.setCol('h');
      }

      if (move.isCastleQ())
      {
         coordinate.setCol('a');
      }

      coordinate.setRow(piece.isWhite() ? '1':'8');
      Piece * pRook = board.getPiece(coordinate);

      // Check that the piece is a Rook
      if (pRook->getName() != 'R')
      {
         return false;
      }

      // Check that the rook has not moved.
      if (pRook->hasMoved())
      {
         return false;
      }

      if (move.isCastleK())
      {
         // Check the spaces between the rook and the king are empty
         for (int i = 5; i < 7; i++)
         {
            if(isLocationOccupied(i, piece.isWhite() ? 7:0))
               return false;
         }
      }

      if (move.isCastleQ())
      {
         // Check the spaces between the rook and the king are empty
         for (int i = 3; i > 0; i--)
         {
            if(isLocationOccupied(i, piece.isWhite() ? 7:0))
               return false;
         }
      }
   }

   // King is moving correct distance.
   return true;
}

/**
 * Validate Queen movement rules.
 */
bool PieceManager::isValidMove(Queen & piece, Move move)
{
   Coordinate src = move.getSrc();
   Coordinate dest = move.getDest();

   int fromRow = board.getRowFromLabel(src.getRow());
   int toRow = board.getRowFromLabel(dest.getRow());
   int fromCol = board.getColFromLabel(src.getCol());
   int toCol = board.getColFromLabel(dest.getCol());
   int rowDistance = fromRow - toRow; // How far are we moving on the Y
   int colDistance = fromCol - toCol; // How far are we moving on the X

   // Get direction Queen is moving.
   Direction direction = getDirection(colDistance, rowDistance);

   // is there anything in the way.
   switch (direction)
   {
      case NorthEast:
         for (int row = fromRow-1, col = fromCol+1; row > toRow; row--, col++)
         {
            // Only stop if we run into something.
            if(isLocationOccupied(col, row)) {
               return false;
            }
         }
         break;

      case NorthWest:
         for (int row = fromRow-1, col = fromCol-1; row > toRow; row--, col--)
         {
            // Only stop if we run into something.
            if(isLocationOccupied(col, row)) {
               return false;
            }
         }
         break;

      case SouthEast:
         for (int row = fromRow+1, col = fromCol+1; row > toRow; row++, col++)
         {
            // Only stop if we run into something.
            if(isLocationOccupied(col, row)) {
               return false;
            }
         }
         break;

      case SouthWest:
         for (int row = fromRow+1, col = fromCol-1; row > toRow; row++, col--)
         {
            // Only stop if we run into something.
            if(isLocationOccupied(col, row)) {
               return false;
            }
         }
         break;

      case North:
         for (int row = fromRow - 1; row > toRow; row--) {
            // Only stop if we run into something.
            if(isLocationOccupied(fromCol, row)) {
               return false;
            }
         }
         break;

      case South:
         for (int row = fromRow + 1; row < toRow; row++) {
            // Only stop if we run into something.
            if(isLocationOccupied(fromCol, row)) {
               return false;
            }
         }
         break;

      case West:
         for (int col = fromCol - 1; col > toCol; col--) {
            // Only stop if we run into something.
            if(isLocationOccupied(col, fromRow)) {
               return false;
            }
         }
         break;

      case East:
         for (int col = fromCol + 1; col < toCol; col++) {
            // Only stop if we run into something.
            if(isLocationOccupied(col, fromRow)) {
               return false;
            }
         }
         break;
   }

   // Queen is moving correct distance. Nothing is in the way.
   return true;

}

/**
 * Validate Bishop movement rules.
 */
bool PieceManager::isValidMove(Bishop & piece, Move move)
{
   Coordinate src = move.getSrc();
   Coordinate dest = move.getDest();

   int fromRow = board.getRowFromLabel(src.getRow());
   int toRow = board.getRowFromLabel(dest.getRow());
   int fromCol = board.getColFromLabel(src.getCol());
   int toCol = board.getColFromLabel(dest.getCol());
   int rowDistance = fromRow - toRow; // How far are we moving on the Y
   int colDistance = fromCol - toCol; // How far are we moving on the X

   // Bishop can only move up and down and left to right an equal distance.
   if(abs(rowDistance) - abs(colDistance) != 0)
   {
      return false; // Bishop is not moving correctly.
   }

   // Get direction bishop is moving.
   Direction direction = getDirection(colDistance, rowDistance);

   // is there anything in the way.
   switch (direction)
   {
      case NorthEast:
         for (int row = fromRow-1, col = fromCol+1; row > toRow; row--, col++)
         {
            // Only stop if we run into something.
            if(isLocationOccupied(col, row)) {
               return false;
            }
         }
         break;

      case NorthWest:
         for (int row = fromRow-1, col = fromCol-1; row > toRow; row--, col--)
         {
            // Only stop if we run into something.
            if(isLocationOccupied(col, row)) {
               return false;
            }
         }
         break;

      case SouthEast:
         for (int row = fromRow+1, col = fromCol+1; row > toRow; row++, col++)
         {
            // Only stop if we run into something.
            if(isLocationOccupied(col, row)) {
               return false;
            }
         }
         break;

      case SouthWest:
         for (int row = fromRow+1, col = fromCol-1; row > toRow; row++, col--)
         {
            // Only stop if we run into something.
            if(isLocationOccupied(col, row)) {
               return false;
            }
         }
         break;
   }

   // Bishop is moving correct distance. Nothing is in the way.
   return true;

}

/**
 * Validate Knight movement rules.
 */
bool PieceManager::isValidMove(Knight & piece, Move move)
{
   // get the move into something we gan use
   Coordinate src = move.getSrc();
   Coordinate dest = move.getDest();

   // get move coordinates as ints
   int fromRow = board.getRowFromLabel(move.getSrc().getRow());
   int toRow = board.getRowFromLabel(move.getDest().getRow());
   int fromCol = board.getColFromLabel(move.getSrc().getCol());
   int toCol = board.getColFromLabel(move.getDest().getCol());

   // set up offsets for possible knight moves
   // There are eight possible move locations 
   // from the knights current position
   //      0 1
   //     2   3
   //       k
   //     4   5
   //      6 7
   // All possible move locations (even invalid ones)
   int locations[8][2] = {
                  {fromRow + 2, fromCol - 1},
                  {fromRow + 2, fromCol + 1},
                  {fromRow + 1, fromCol - 2},
                  {fromRow + 1, fromCol + 2},
                  {fromRow -1, fromCol - 2},
                  {fromRow -1, fromCol + 2},
                  {fromRow -2, fromCol -1},
                  {fromRow -2, fromCol + 1}
             };
   // loop through move list
   for (int i = 0;i <= 8; i++)
      {
         int row = locations[i][0]; // to make code pretty
         int col = locations[i][1]; // to make code pretty
         // get the place where the piece is trying to move
         Piece * pTo = board.getPiece(move.getDest());
         // check to see if the dest matched the value in the array
         if (row == toRow && col == toCol) // the to location is valid
         {
            return true;
         }
      }
   return false;
}

/**
 * Validate Rook movement rules.
 */
bool PieceManager::isValidMove(Rook & piece, Move move)
{
   Coordinate src = move.getSrc();
   Coordinate dest = move.getDest();

   int fromRow = board.getRowFromLabel(src.getRow());
   int toRow = board.getRowFromLabel(dest.getRow());
   int fromCol = board.getColFromLabel(src.getCol());
   int toCol = board.getColFromLabel(dest.getCol());
   int rowDistance = fromRow - toRow; // How far are we moving on the Y
   int colDistance = fromCol - toCol; // How far are we moving on the X

   if(rowDistance != 0 && colDistance != 0)
   {
      return false; // Rook is not moving correctly.
   }

   // Get direction Rook is moving.
   Direction direction = getDirection(colDistance, rowDistance);

   switch (direction)
   {
      case North:
         for (int row = fromRow - 1; row > toRow; row--) {
            // Only stop if we run into something.
            if(isLocationOccupied(fromCol, row)) {
               return false;
            }
         }
         break;

      case South:
         for (int row = fromRow + 1; row < toRow; row++) {
            // Only stop if we run into something.
            if(isLocationOccupied(fromCol, row)) {
               return false;
            }
         }
        break;

      case West:
         for (int col = fromCol - 1; col > toCol; col--) {
            // Only stop if we run into something.
            if(isLocationOccupied(col, fromRow)) {
               return false;
            }
         }
        break;

      case East:
         for (int col = fromCol + 1; col < toCol; col++) {
            // Only stop if we run into something.
            if(isLocationOccupied(col, fromRow)) {
               return false;
            }
         }
        break;
   }

   // Rook is moving correct distance. Nothing is in the way.
   return true;
}

/*
 * Gets all the available moves based on the piece entered
 */
std::vector<Move> PieceManager::getAvailableMoves(Coordinate coordinate)
{
   std::vector<Move> moves;
   Piece * pFrom = board.getPiece(coordinate);

   switch (pFrom->getName())
   {
      case 'P':
         return this->getAvailableMoves((Pawn &) *pFrom, coordinate); // Foster

      case 'K':
         return this->getAvailableMoves((King &) *pFrom, coordinate); // Nick

      case 'B':
         return this->getAvailableMoves((Bishop &) *pFrom, coordinate); // Nick

      case 'N':
         return this->getAvailableMoves((Knight &) *pFrom, coordinate); // Foster

      case 'Q':
         return this->getAvailableMoves((Queen &) *pFrom, coordinate); // Tyler

      case 'R':
         return this->getAvailableMoves((Rook &) *pFrom, coordinate); // Tyler

      default:
         return moves; // If Space return an empty list. This is how the demo works.
   }

}

/*
 * Gets all the available moves for the Pawn
 */
std::vector<Move> PieceManager::getAvailableMoves(Pawn & piece, Coordinate src)
{
   // set the move direction for the rows
   // if it the piece it white it will move up the board (-1 row)
   // if the piece is black it moves down the board (+1 row)
   int moveDirection = (piece.isWhite()) ? -1 : 1;

   // set up offsets for possible pawn moves
   // There are three possible move locations
   // a pawns current position
   //     0 1 2
   //       p
   //     0 1 2
   // we will set a move direction to get the correct offsets

   std::vector<Move> moves;
   Move moveLeft, moveCenter, moveCenterTwo, moveRight;

   // Center
   Coordinate dest = getCoordByOffSet(src, 0, moveDirection); // 1
   if (dest.isValid())
   {
      moveCenter.setText(src, dest);
      addMove(&moves, moveCenter);
   }

   // Center x2
   Coordinate dest2 = getCoordByOffSet(src, 0, moveDirection+moveDirection); // 1 + 1
   if (dest2.isValid())
   {
      moveCenterTwo.setText(src, dest2);
      addMove(&moves, moveCenterTwo);
   }

   // Right
   Coordinate edest = getCoordByOffSet(src, 1, moveDirection);  // 2
   if (edest.isValid())
   {
      moveRight.setText(src, edest);
      addMove(&moves, moveRight);
   }

   // Left
   Coordinate wdest = getCoordByOffSet(src, -1, moveDirection); // 0
   if (wdest.isValid())
   {
      moveLeft.setText(src, wdest);
      addMove(&moves, moveLeft);
   }

   if(dest.getRow() == '1' || dest.getRow() == '8') // pawns can never get to their own back rank. So no need to check color.
   {
      // promotions available
      char promotions[5] = "QRBN";
      for (int i = 0; i < 4; i++)
      {
         char arr[6];
         arr[0] = src.getCol();
         arr[1] = src.getRow();
         arr[4] = promotions[i];

         // three promotes possibilities
         Move promoteLeft, promoteCenter, promoteRight;
         if (moveCenter.isValid())
         {
            arr[2] = moveCenter.getDest().getCol();
            arr[3] = moveCenter.getDest().getRow();
            std::string name(arr);
            promoteCenter.setText(name.substr(0,5));
            addMove(&moves, promoteCenter);
         }

         if (moveLeft.isValid())
         {
            arr[2] = moveLeft.getDest().getCol();
            arr[3] = moveLeft.getDest().getRow();
            std::string name(arr);
            promoteLeft.setText(name.substr(0,5));
            addMove(&moves, promoteLeft);
         }

         if (moveRight.isValid())
         {
            arr[2] = moveRight.getDest().getCol();
            arr[3] = moveRight.getDest().getRow();
            std::string name(arr);
            promoteRight.setText(name.substr(0,5));
            addMove(&moves, promoteRight);
         }
      }
   }

   // two enpassant possibilities - one on either side of piece
   Move enpassant;
   Coordinate captureEast = getCoordByOffSet(src, 1, moveDirection);
   if (captureEast.isValid())
   {
      char arr[5];
      arr[0] = src.getCol();
      arr[1] = src.getRow();
      arr[2] = captureEast.getCol();
      arr[3] = captureEast.getRow();
      arr[4] = 'E';
      std::string name(arr);
      enpassant.setText(name.substr(0,5));
      addMove(&moves, enpassant);
   }

   Coordinate captureWest = getCoordByOffSet(src, -1, moveDirection);
   if (captureWest.isValid())
   {
      char arr[5];
      arr[0] = src.getCol();
      arr[1] = src.getRow();
      arr[2] = captureWest.getCol();
      arr[3] = captureWest.getRow();
      arr[4] = 'E';
      std::string name(arr);
      enpassant.setText(name.substr(0,5));
      addMove(&moves, enpassant);
   }

   return moves;
}

/**
 * Get all the valid moves for a king.
 * A king moves one space in each direction.
 */
std::vector<Move> PieceManager::getAvailableMoves(King & piece, Coordinate src)
{
   std::vector<Move> moves;

   // N
   Coordinate nDest = getCoordByOffSet(src, 0, -1);
   addMove(&moves, src, nDest);

   // NE
   Coordinate neDest = getCoordByOffSet(src, 1, -1);
   addMove(&moves, src, neDest);

   // E
   Coordinate eDest = getCoordByOffSet(src, 1, 0);
   addMove(&moves, src, eDest);

   // SE
   Coordinate seDest = getCoordByOffSet(src, 1, 1);
   addMove(&moves, src, seDest);

   // S
   Coordinate sDest = getCoordByOffSet(src, 0, 1);
   addMove(&moves, src, sDest);

   // SW
   Coordinate swDest = getCoordByOffSet(src, -1, 1);
   addMove(&moves, src, swDest);

   // W
   Coordinate wDest = getCoordByOffSet(src, -1, 0);
   addMove(&moves, src, wDest);

   // NW
   Coordinate nwDest = getCoordByOffSet(src, -1, -1);
   addMove(&moves, src, nwDest);

   // Check castle moves
   if(!piece.hasMoved())
   {
      Move castleK, castleQ;
      castleK.setText(piece.isWhite() ? "e1g1c" : "e8g8c");
      castleQ.setText(piece.isWhite() ? "e1c1C" : "e8c8C");
      addMove(&moves, castleK);
      addMove(&moves, castleQ);
   }

   return moves;
}

/**
 * Get all the valid moves for a queen.
 * A queen moves many spaces in each direction until the edge of the board.
 */
std::vector<Move> PieceManager::getAvailableMoves(Queen & piece, Coordinate src)
{
   std::vector<Move> moves;

   // E
   int x = 1;
   int y = 0;
   while(true)
   {
      Coordinate seDest = getCoordByOffSet(src, x++, y);
      if(!addMove(&moves, src, seDest)) break;
   }

   // N
   x = 0;
   y = -1;
   while(true)
   {
      Coordinate neDest = getCoordByOffSet(src, x, y--);
      if(!addMove(&moves, src, neDest)) break;
   }

   // S
   x = 0;
   y = 1;
   while(true)
   {
      Coordinate swDest = getCoordByOffSet(src, x, y++);
      if(!addMove(&moves, src, swDest)) break;
   }

   // W
   x = -1;
   y = 0;
   while(true)
   {
      Coordinate nwDest = getCoordByOffSet(src, x--, y);
      if(!addMove(&moves, src, nwDest)) break;
   }

   // NE
   x = 1;
   y = -1;
   while(true)
   {
      Coordinate neDest = getCoordByOffSet(src, x++, y--);
      if(!addMove(&moves, src, neDest)) break;
   }

   // SE
   x = 1;
   y = 1;
   while(true)
   {
      Coordinate seDest = getCoordByOffSet(src, x++, y++);
      if(!addMove(&moves, src, seDest)) break;
   }

   // SW
   x = -1;
   y = 1;
   while(true)
   {
      Coordinate swDest = getCoordByOffSet(src, x--, y++);
      if(!addMove(&moves, src, swDest)) break;
   }

   // NW
   x = -1;
   y = -1;
   while(true)
   {
      Coordinate nwDest = getCoordByOffSet(src, x--, y--);
      if(!addMove(&moves, src, nwDest)) break;
   }

   return moves;
}

/**
 * Get all the valid moves for a bishop.
 * A bishop moves diagonally in each direction until the edge of the board.
 */
std::vector<Move> PieceManager::getAvailableMoves(Bishop & piece, Coordinate src)
{
   std::vector<Move> moves;

   // NE
   int x = 1;
   int y = -1;
   while(true)
   {
      Coordinate neDest = getCoordByOffSet(src, x++, y--);
      if(!addMove(&moves, src, neDest)) break;
   }

   // SE
   x = 1;
   y = 1;
   while(true)
   {
      Coordinate seDest = getCoordByOffSet(src, x++, y++);
      if(!addMove(&moves, src, seDest)) break;
   }

   // SW
   x = -1;
   y = 1;
   while(true)
   {
      Coordinate swDest = getCoordByOffSet(src, x--, y++);
      if(!addMove(&moves, src, swDest)) break;
   }

   // NW
   x = -1;
   y = -1;
   while(true)
   {
      Coordinate nwDest = getCoordByOffSet(src, x--, y--);
      if(!addMove(&moves, src, nwDest)) break;
   }

   return moves;
}

/*
 * Gets all available moves for the Knight
 */
std::vector<Move> PieceManager::getAvailableMoves(Knight & piece, Coordinate src)
{
   // set up offsets for possible knight moves
   // There are eight possible move locations
   // from the knights current position
   //      0 1
   //     2   3
   //       k
   //     4   5
   //      6 7
   std::vector<Move> moves;
   int locations[8][2] = {
                  {-2, -1},
                  {-2,  1},
                  {-1, -2},
                  {-1,  2},
                  { 1, -2},
                  { 1,  2},
                  { 2, -1},
                  { 2,  1}
             };
   // loop through move list
   for (int i = 0;i <= 8; i++)
      {
         int row = locations[i][0]; // to make code pretty
         int col = locations[i][1]; // to make code pretty
         // get the place where the piece is trying to move
         Coordinate Dest = getCoordByOffSet(src, col, row);
         addMove(&moves, src, Dest);
      }

   return moves;

}

/*
 * Gets all the available moves for the Rook
 */
std::vector<Move> PieceManager::getAvailableMoves(Rook & piece, Coordinate src)
{
   std::vector<Move> moves;

   // E
   int x = 1;
   int y = 0;
   while(true)
   {
      Coordinate seDest = getCoordByOffSet(src, x++, y);
      if(!addMove(&moves, src, seDest)) break;
   }

   // N
   x = 0;
   y = -1;
   while(true)
   {
      Coordinate neDest = getCoordByOffSet(src, x, y--);
      if(!addMove(&moves, src, neDest)) break;
   }

   // S
   x = 0;
   y = 1;
   while(true)
   {
      Coordinate swDest = getCoordByOffSet(src, x, y++);
      if(!addMove(&moves, src, swDest)) break;
   }

   // W
   x = -1;
   y = 0;
   while(true)
   {
      Coordinate nwDest = getCoordByOffSet(src, x--, y);
      if(!addMove(&moves, src, nwDest)) break;
   }

   return moves;
}

/**
 * Check movement to determine direction. In this case the Y axis is flipped.
 */
PieceManager::Direction PieceManager::getDirection(int colDistance, // How far are we moving on the X
                                                   int rowDistance) // How far are we moving on the Y
{
   assert(colDistance >= -7 && rowDistance >= -7 && colDistance <= 7 && rowDistance <= 7); // Validate Range
   if(colDistance > 0 && rowDistance == 0)
      return West;
   if(colDistance < 0 && rowDistance == 0)
      return East;
   if(rowDistance > 0 && colDistance == 0)
      return North;
   if(rowDistance < 0 && colDistance == 0)
      return South;
   if((colDistance+rowDistance) > 0)
      return NorthWest;
   if((colDistance+rowDistance) < 0)
      return SouthEast;
   if((colDistance-rowDistance) > 0)
      return SouthWest;
   if((colDistance-rowDistance) < 0)
      return NorthEast;

   throw "Invalid direction";
}

/**
 * Get the coords from a src by an offset.
 */
Coordinate PieceManager::getCoordByOffSet(Coordinate src, int x_col, int y_row)
{
//   assert(x_col > -8 && y_row > -8 && x_col < 8 && y_row < 8); // Validate Range
   Coordinate dest;

   int row = board.getRowFromLabel(src.getRow()) + y_row;
   int col = board.getColFromLabel(src.getCol()) + x_col;
   dest.setRow(board.getRowLabel(row));
   dest.setCol(board.getColLabel(col));
   return dest;
}

/**
 * Add a dest to a move list, if it is valid.
 * returns true if the move was valid and added.
 */
bool PieceManager::addMove(std::vector<Move> * moves, Coordinate src, Coordinate dest)
{
   if(dest.isValid())
   {
      Piece * pFrom = board.getPiece(src);
      Piece * pTo = board.getPiece(dest);

      // check if the dest is occupied and is on the same team.
      if (!pTo->isSpace() && (pFrom->isWhite() == pTo->isWhite()))
      {
         return false;
      }

      Move move;
      if (pTo->isSpace())
      {
         move.setText(src, dest); // setup empty spaces.
      }
      else
      {
         // Do not allow capturing of the king.
         if (pTo->getName() == 'K') return false;

         move.setText(src, dest, *pTo); // handle moves that have enemy team on them.
      }

      if (isValidMove(move))
      {
         moves->push_back(move);
         return true;
      }
   }
   return false;
}

/**
 * Add a dest to a move list, if it is valid.
 * returns true if the move was valid and added.
 *
 * Needed to check moves with other flags set. Mostly used for special moves.
 */
bool PieceManager::addMove(std::vector<Move> * moves, Move & move)
{
   if(move.getDest().isValid())
   {
      Piece * pFrom = board.getPiece(move.getSrc());
      Piece * pTo = board.getPiece(move.getDest());

      // check if the dest is occupied and is on the same team.
      if (!pTo->isSpace() && (pFrom->isWhite() == pTo->isWhite()))
      {
         return false;
      }
      else if (!pTo->isSpace() && !move.isEnpassant()) // handle moves that have enemy team on them.
      {
         if(move.isPromote())
         {
            move.setText(move.getSrc(), move.getDest(), * pTo, move.getPromoteTo());
         }
         else
         {
            move.setText(move.getSrc(), move.getDest(), * pTo);
         }
      }

      if (isValidMove(move))
      {
         moves->push_back(move);
         return true;
      }
   }
   return false;
}

/***********************************************
* CheckSpace   check to see what is on a space
* input Coordinate
* output SPACE, BLACK, WHITE
***********************************************/
int PieceManager::checkLocation(int row, int col)
{
   assert(row >= 0 && row <= 7 && col >= 0 && col <= 7); // Validate Range
   Coordinate coordinate;
   coordinate.setRow(board.getRowLabel(row));
   coordinate.setCol(board.getColLabel(col));
   Piece * pCheck = board.getPiece(coordinate);
   if(pCheck->isSpace())
      return SPACE;
   return (pCheck->isWhite()) ? WHITE : BLACK;
}

/**
 * Check if a location is occupied.
 */
bool PieceManager::isLocationOccupied(int col, int row)
{
   assert(row >= 0 && row <= 7 && col >= 0 && col <= 7); // Validate Range
   Coordinate coordinate;
   coordinate.setRow(board.getRowLabel(row));
   coordinate.setCol(board.getColLabel(col));
   Piece * pCheck = board.getPiece(coordinate);
   return !pCheck->isSpace();
}
