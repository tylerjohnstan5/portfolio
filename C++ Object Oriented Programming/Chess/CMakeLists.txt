cmake_minimum_required(VERSION 3.3)
project(Procedural_Chess)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

set(SOURCE_FILES main.cpp Board.cpp Board.h Piece.cpp
        Piece.h Space.cpp Space.h King.cpp King.h Queen.cpp
        Queen.h Rook.cpp Rook.h Bishop.cpp Bishop.h Knight.cpp
        Knight.h Pawn.cpp Pawn.h Coordinate.cpp Coordinate.h
        Game.cpp Game.h PieceManager.cpp PieceManager.h
        Move.cpp Move.h)
add_executable(Procedural_Chess ${SOURCE_FILES})
