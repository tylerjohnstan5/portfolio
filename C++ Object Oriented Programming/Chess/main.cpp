#include <iostream>
#include "Board.h"
#include "Game.h"

using namespace std;

/**
 * Main entry point.
 */
int main() {

   Board board; // Create board
   PieceManager pieceManager(board); // Create PieceManager and give it access to the board.
   Game game(board, pieceManager); //  Create game and give it to the game.
   game.run();
   return 0;
}
