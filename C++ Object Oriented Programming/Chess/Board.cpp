#include "Board.h"
#include "PieceManager.h"

/*
 * Initializes the board
 */
Board::Board()
{
   testMode = false; // Default to false;
   newGame();
}

/*
 * Prints the board
 */
void Board::printWriteSquare(int row, int col) {

   assert(row >= 0 && row <= 7 && col >= 0 && col <= 7); // Validate Range

   char name = formatPieceName(this->boardGrid[row][col]->getName(), this->boardGrid[row][col]->isWhite());

   if (isTestMode())
   {
      std::cout << name;
   }
   else if (this->boardGrid[row][col]->isWhite())
   {
      //std::cout << "\E[31;47m" << " R " << "\E[0m"; // white square, white piece
      std::cout << "\E[31;47m" << " " << name  << " " << "\E[0m"; // white square, white piece
   }
   else
   {
      //std::cout << "\E[30;47m" << " R " << "\E[0m"; // white square, black piece
      std::cout << "\E[30;47m" << " " << name  << " " << "\E[0m"; // white square, black piece
   }
}

/*
 * Prints the Red Square
 */
void Board::printRedSquare(int row, int col) {

   assert(row >= 0 && row <= 7 && col >= 0 && col <= 7); // Validate Range

   char name = formatPieceName(this->boardGrid[row][col]->getName(), this->boardGrid[row][col]->isWhite());

   if (isTestMode()) //displays the test board
   {
      std::cout << name;
   }
   else if (this->boardGrid[row][col]->isWhite()) //otherwise display normal board
   {
      //std::cout << "\E[37;41m" << " R " << "\E[0m"; // black square, white piece
      std::cout << "\E[37;41m" << " " << name  << " " << "\E[0m"; // black square, white piece
   }
   else
   {
      //std::cout << "\E[30;41m" << " R " << "\E[0m"; // black square, black piece
      std::cout << "\E[30;41m" << " " << name  << " " << "\E[0m"; // black square, black piece
   }
}

/*
 *  Prints the board
 */
void Board::printBoard() {
   if(!isTestMode())
   {
      this->clearScreen();
   }

   this->printHeader();
   for(int i = 0; i <= 7; i++) {
      this->printRowThatStartsWithWhite(i);
      this->printRowThatStartsWithBlack(++i);
   }
   if(!isTestMode())
   {
      this->printHeader();
   }
}

/*
 * Clears the screen after each move
 */
void Board::clearScreen() {
   std::cout << "\E[H\E[2J";
}

/**
 * Prints the board header
 */
void Board::printHeader() {

   if(isTestMode())
   {
      std::cout << "  abcdefgh" << std::endl;
   }
   else
   {
      std::cout << "   a  b  c  d  e  f  g  h " << std::endl;
   }
}

/*
 *  Prints the row that starts with Black
 */
void Board::printRowThatStartsWithBlack(int row) { //code used to help display the board correctly
   std::cout << getRowLabel(row) << " ";

   for(int i = 0; i < 8; i++)
   {
      if(i == 0 || i % 2 == 0)
      {
         this->printRedSquare(row, i);
      }
      else
      {
         this->printWriteSquare(row, i);
      }
   }

   if(isTestMode())
   {
      std::cout << std::endl;
   }
   else
   {
      std::cout << " " << getRowLabel(row) << std::endl;
   }
}

/*
 * Prints the row that starts with White
 */
void Board::printRowThatStartsWithWhite(int row) {
   std::cout << getRowLabel(row) << " ";

   for(int i = 0; i < 8; i++)
   {
      if(i == 0 || i % 2 == 0)
      {
         this->printWriteSquare(row, i);
      }
      else
      {
         this->printRedSquare(row, i);
      }
   }

   if(isTestMode())
   {
      std::cout << std::endl;
   }
   else
   {
      std::cout << " " << getRowLabel(row) << std::endl;
   }
}

/*
 * Gets the Row Label
 */
char Board::getRowLabel(int row)
{
   switch (row)
   {
      case 0:
         return '8';
      case 1:
         return '7';
      case 2:
         return '6';
      case 3:
         return '5';
      case 4:
         return '4';
      case 5:
         return '3';
      case 6:
         return '2';
      case 7:
         return '1';
      default:
         return 'E'; // Error
   }
}

/*
 * Gets the row from the label
 */
int Board::getRowFromLabel(char row)
{
   switch (row)
   {
      case '8':
         return 0;
      case '7':
         return 1;
      case '6':
         return 2;
      case '5':
         return 3;
      case '4':
         return 4;
      case '3':
         return 5;
      case '2':
         return 6;
      case '1':
         return 7;
      default:
         return 'E'; // Error
   }
}

/*
 * Gets the Col Label
 */
char Board::getColLabel(int col)
{
   switch (col)
   {
      case 0:
         return 'a';
      case 1:
         return 'b';
      case 2:
         return 'c';
      case 3:
         return 'd';
      case 4:
         return 'e';
      case 5:
         return 'f';
      case 6:
         return 'g';
      case 7:
         return 'h';
      default:
         return 'E'; // Error
   }
}

/*
 * Gets the Col from the Label
 */
int Board::getColFromLabel(char col)
{
   switch (col)
   {
      case 'a':
         return 0;
      case 'b':
         return 1;
      case 'c':
         return 2;
      case 'd':
         return 3;
      case 'e':
         return 4;
      case 'f':
         return 5;
      case 'g':
         return 6;
      case 'h':
         return 7;
      default:
         return 'E'; // Error
   }
}

/*
 * Checks if the board is in test mode
 */
bool Board::isTestMode()
{
   return testMode;
}

/*
 * Toggles the test mode
 */
void Board::toggleTestMode()
{
   testMode = !isTestMode();
}

/*
 *  Formats the pieces based on color
 */
char Board::formatPieceName(char name, bool isWhite)
{
   if (isTestMode())
   {
      return (char) (isWhite ? tolower(name) : name);
   }

   switch (name)
   {
      case 'P':
         return 'p';

      default:
         return name;
   }
}

/**
 * Get a piece from the board based on the cords passed.
 */
Piece * Board::getPiece(Coordinate cords)
{
   return boardGrid[getRowFromLabel(cords.getRow())][getColFromLabel(cords.getCol())];
}

/*
 * Removes the piece from the board
 */
void Board::removePiece(Coordinate cords)
{
   boardGrid[getRowFromLabel(cords.getRow())][getColFromLabel(cords.getCol())] = new Space();
}

/**
 * Moves a piece on the board and removes it from it's old place.
 * Sets has moved flag on piece.
 *
 * Handles Castle. En-Passant, and Promotions.
 */
void Board::movePiece(Move move)
{
   Piece * pFrom = getPiece(move.getSrc());
   boardGrid[getRowFromLabel(move.getDest().getRow())][getColFromLabel(move.getDest().getCol())] = pFrom;
   removePiece(move.getSrc());
   pFrom->setMoved();

   // check if we need to handle special castle moves.
   handleCastle(move, pFrom);
   handleEnpassant(move);
   handlePromote(move, pFrom);
}

/*
 * Starts a new game
 */
void Board::newGame()
{
   // Init board and setup new game.
   for(int i = 0; i < 8; i++){
      if(i == 0 || i == 7)
      {
         this->boardGrid[i][0] = new Rook(i == 7);
         this->boardGrid[i][1] = new Knight(i == 7);
         this->boardGrid[i][2] = new Bishop(i == 7);
         this->boardGrid[i][3] = new Queen(i == 7);
         this->boardGrid[i][4] = new King(i == 7);
         this->boardGrid[i][5] = new Bishop(i == 7);
         this->boardGrid[i][6] = new Knight(i == 7);
         this->boardGrid[i][7] = new Rook(i == 7);
      }
      else if (i == 1 || i == 6) {
         for(int j = 0; j < 8; j++)
         {
            this->boardGrid[i][j] = new Pawn(i == 6);
         }
      }
      else
      {
         for(int j = 0; j < 8; j++)
         {
            this->boardGrid[i][j] = new Space();
         }
      }
   }
}

/**
 * Clean up the piece objects.
 */
Board::~Board()
{
   for(int i = 0; i < 8; i++){
      for(int j = 0; j < 8; j++){
         delete this->boardGrid[i][j];
      }
   }
}

/**
 * Overloaded extraction operator that prints the board to the screen.
 */
std::ostream & operator << (std::ostream & out, Board & rhs)
{
   rhs.printBoard();
   return out;
}

/**
 * Deal with the castle. This moves a rook. All validation is already done.
 */
void Board::handleCastle(Move & move, Piece * piece)
{
   if (!move.isCastle()) return;

   if (move.isCastleK())
   {
      Move moveRook;
      moveRook.setText(piece->isWhite() ? "h1f1" : "h8f8");
      movePiece(moveRook);
   }

   if (move.isCastleQ())
   {
      Move moveRook;
      moveRook.setText(piece->isWhite() ? "a1d1" : "a8d8");
      movePiece(moveRook);
   }

}

/**
 * Deal with the enpassant removing a piece (captured it)castle. 
 * All validation is already done.
 */
void Board::handleEnpassant(Move & move)
{
   if (!move.isEnpassant()) return;
   
   if (move.isEnpassant())
   {
      // get the location of the piece to capture
      // it is the fromRow and the toCol
      int toCol = getColFromLabel(move.getDest().getCol());
      int fromRow = getRowFromLabel(move.getSrc().getRow());

      Coordinate takeMe;
      takeMe.setCol(getColLabel(toCol));
      takeMe.setRow(getRowLabel(fromRow));
      removePiece(takeMe);  
   }
}

/**
 * Deal with the promotion change the pawn to something else.
 * All validation is already done.
 */
void Board::handlePromote(Move & move, Piece * piece)
{
   if (!move.isPromote()) return;

   // get the location of the piece to capture
   // it is the fromRow and the toCol
   int toCol = getColFromLabel(move.getDest().getCol());
   int toRow = getRowFromLabel(move.getDest().getRow());

   switch (move.getPromoteTo())
   {
      case 'R':
         this->boardGrid[toRow][toCol] = new Rook(piece->isWhite());
         break;
      case 'N':
         this->boardGrid[toRow][toCol] = new Knight(piece->isWhite());
         break;
      case 'B':
         this->boardGrid[toRow][toCol] = new Bishop(piece->isWhite());
         break;
      case 'Q':
         this->boardGrid[toRow][toCol] = new Queen(piece->isWhite());
         break;
   }
}

/**
 * Gets the Space Rank.
 */
float Board::getRankSpace()
{
   int rankSpaceGrid[8][8];

   for (int r = 0, c = 0; c < 8; c++, r=0)
   {
      if (c == 0 || c == 7)
      {
         // A & H
         rankSpaceGrid[c][r++] = 0;
         rankSpaceGrid[c][r++] = 0;
         rankSpaceGrid[c][r++] = 1;
         rankSpaceGrid[c][r++] = 2;
         rankSpaceGrid[c][r++] = 2;
         rankSpaceGrid[c][r++] = 1;
         rankSpaceGrid[c][r++] = 0;
         rankSpaceGrid[c][r] = 0;
      }
      else if (c == 1 || c == 6)
      {
         // B & G
         rankSpaceGrid[c][r++] = 0;
         rankSpaceGrid[c][r++] = 1;
         rankSpaceGrid[c][r++] = 2;
         rankSpaceGrid[c][r++] = 3;
         rankSpaceGrid[c][r++] = 3;
         rankSpaceGrid[c][r++] = 2;
         rankSpaceGrid[c][r++] = 1;
         rankSpaceGrid[c][r] = 0;
      }
      else if (c == 2 || c == 5)
      {
         // C & F
         rankSpaceGrid[c][r++] = 1;
         rankSpaceGrid[c][r++] = 2;
         rankSpaceGrid[c][r++] = 3;
         rankSpaceGrid[c][r++] = 4;
         rankSpaceGrid[c][r++] = 4;
         rankSpaceGrid[c][r++] = 3;
         rankSpaceGrid[c][r++] = 2;
         rankSpaceGrid[c][r] = 1;
      }
      else
      {
         // D & E
         rankSpaceGrid[c][r++] = 2;
         rankSpaceGrid[c][r++] = 3;
         rankSpaceGrid[c][r++] = 4;
         rankSpaceGrid[c][r++] = 4;
         rankSpaceGrid[c][r++] = 4;
         rankSpaceGrid[c][r++] = 4;
         rankSpaceGrid[c][r++] = 3;
         rankSpaceGrid[c][r] = 2;
      }
   }

   float whiteScore = 0, blackScore = 0;
   for(int col = 0; col < 8; col++){
      for(int row = 0; row < 8; row++)
      {
         if(!this->boardGrid[col][row]->isSpace())
         {
            int val = rankSpaceGrid[col][row];

            if (this->boardGrid[col][row]->isWhite())
            {
               // add to whites score.
               whiteScore += val;
            }
            else
            {
               // Add to blacks score.
               blackScore += val;
            }
         }
      }
   }
   return (float) ((whiteScore - blackScore) / 10.0);
}

float Board::getRankForce()
{
   float whiteScore = 0, blackScore = 0;
   for(int col = 0; col < 8; col++){
      for(int row = 0; row < 8; row++)
      {
         if(!this->boardGrid[col][row]->isSpace())
         {
            int val = this->boardGrid[col][row]->getScore();

            if (this->boardGrid[col][row]->isWhite())
            {
               // add to whites score.
               whiteScore += val;
            }
            else
            {
               // Add to blacks score.
               blackScore += val;
            }
         }
      }
   }
   return (float) ((whiteScore - blackScore) / 10.0);
}
