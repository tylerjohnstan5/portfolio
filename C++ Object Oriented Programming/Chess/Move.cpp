#include "Move.h"

/**
 * This files is used to enable each piece on the board to move.
 * including the special moves such as enpassant and castling
 */
Move::Move()
{
   reset();
}

/**
 * Gets the source coordinate
 */
Coordinate & Move::getSrc()
{
   return this->source;
}

/**
 * Gets the destination coordinate
 */
Coordinate & Move::getDest()
{
   return this->dest;
}

/**
 * Overloaded extraction operator that prints the move text to the screen.
 */
std::ostream & operator << (std::ostream & out, Move & rhs)
{
   out << rhs.text;
   return out;
}

/**
 * Overloaded insertion operator that sets the text
 */
std::istream & operator >> (std::istream & in, Move & rhs)
{
   std::string text;
   in >> text;
   rhs.setText(text);

   return in;
}

/**
 * returns the plain text value of a move.
 */
std::string Move::getText()
{
   return this->text;
}

/**
 * Set text value
 */
void Move::setText(std::string value)
{
   if(validateInput(value))
   {
      this->reset();
      this->text = value;
      this->update();
   }
}


/**
 * Take a set of cords and set the text value.
 */
void Move::setText(Coordinate & src, Coordinate & dest)
{
   char arr[4];
   arr[0] = src.getCol();
   arr[1] = src.getRow();
   arr[2] = dest.getCol();
   arr[3] = dest.getRow();
   std::string text(arr);
   this->setText(text.substr(0,4));
}

/**
 * Take a set of cords and a piece set the text value to capture the piece.
 */
void Move::setText(Coordinate & src, Coordinate & dest, Piece & toPiece)
{
   char arr[5];
   arr[0] = src.getCol();
   arr[1] = src.getRow();
   arr[2] = dest.getCol();
   arr[3] = dest.getRow();
   arr[4] = (char) tolower(toPiece.getName());
   std::string text(arr);
   this->setText(text.substr(0,5));
}

/**
 * Take a set of cords, a piece and a promotion. set the text value to capture the piece and promote.
 */
void Move::setText(Coordinate & src, Coordinate & dest, Piece & toPiece, char promoteTo)
{
   char arr[6];
   arr[0] = src.getCol();
   arr[1] = src.getRow();
   arr[2] = dest.getCol();
   arr[3] = dest.getRow();
   arr[4] = promoteTo;
   arr[5] = (char) tolower(toPiece.getName());
   std::string text(arr);
   this->setText(text.substr(0,6));
}

/**
 * Notation: <from square><to square>[<capture>][<promotion>][<castling>]
 */
bool Move::validateInput(std::string input) //checks for valid input
{
   // Check for menu items.
   if (input == "?" || input == "quit" || input == "test" || input == "read" || input == "help" || input == "rank")
   {
      this->command = true;
      return true;
   }

   // Check for valid lengths.
   if (input.length() < 4 || input.length() > 6)
   {
      return false;
   }

   // Check for valid values.
   if (!isColIndicatorValid(input[0]))
      throw "Invalid format of source coordinates";
   if (!isRowIndicatorValid(input[1]))
      throw "Invalid format of source coordinates";
   if (!isColIndicatorValid(input[2]))
      throw "Invalid format of destination coordinates";
   if (!isRowIndicatorValid(input[3]))
      throw "Invalid format of destination coordinates";

   // Fifth value can only be [<capture>][<promotion>][<castling>]
   if(input.length() > 4)
   {
      if (!isCaptureIndicatorValid(input[4])
          && !isPromotionIndicatorValid(input[4], input[3])
          && !isCastlingIndicatorValid(input[4]))
      {
         if (!isPromotionIndicatorValid(input[4], input[3]))
            throw "Unknown promotion piece specification";
         if (!isCaptureIndicatorValid(input[4]))
            throw "Bad Capture";
         if (!isCastlingIndicatorValid(input[4]))
            throw "Bad Castle";
      }
      else
      {
         if (isPromotionIndicatorValid(input[4], input[3]))
         {
            this->promote = true;
            setPromoteTo(input[4]);
         }

         if (isCaptureIndicatorValid(input[4]))
         {
            this->capture = true;
            setCapturePieceName(input[4]);
            checkEnpassant(input[4]);
         }

         if (isCastlingIndicatorValid(input[4]))
         {
            checkCastle(input[4]);
         }
      }
   }

   // I think the 6th value can only be promotion but the example shows a bad format where capture is last.
   if(input.length() > 5)
   {
      if (!isCaptureIndicatorValid(input[5])
          && !isPromotionIndicatorValid(input[5], input[3]))
      {
         if (!isCaptureIndicatorValid(input[5]))
            throw "Bad Capture";
         if (!isPromotionIndicatorValid(input[5], input[3]))
            throw "Unknown promotion piece specification";
      }
      else
      {
         if (isPromotionIndicatorValid(input[5], input[3]))
         {
            this->promote = true;
            setPromoteTo(input[5]);
         }

         if (isCaptureIndicatorValid(input[5]))
         {
            this->capture = true;
            setCapturePieceName(input[5]);
            checkEnpassant(input[5]);
         }
      }
   }
   return true;
}

/*
 * Checks that the col indicator are valid
 */
bool Move::isColIndicatorValid(char & value)
{
   return (value == 'a' || value == 'b' || value == 'c' || value == 'd' ||
           value == 'e' || value == 'f' || value == 'g' || value == 'h');
}

/*
 * Checks that the row indicator are valid
 */
bool Move::isRowIndicatorValid(char & value)
{
   return (value == '1' || value == '2' || value == '3' || value == '4' ||
           value == '5' || value == '6' || value == '7' || value == '8');
}

/*
 * Checks if the capture indicator is valid
 */
bool Move::isCaptureIndicatorValid(char & value)
{
   return (value == 'p' || value == 'n' || value == 'b' || value == 'r' ||
           value == 'q' || value == 'e' || value == 'E');
}

/*
 * Checks if the promotion indicator is valid
 */
bool Move::isPromotionIndicatorValid(char & value, char & rowIndicator)
{
   if (rowIndicator != '1' && rowIndicator != '8') return false;
   return (value == 'N' || value == 'B' || value == 'R' || value == 'Q');
}

/*
 * Checks if the the castle indicator is valid
 */
bool Move::isCastlingIndicatorValid(char & value)
{
   return (value == 'c' || value == 'C');
}

/*
 * Checks if the Move is valid.
 */
bool Move::isValid()
{
   return this->validateInput(this->getText());
}

/**
 * Update values if not a command.
 */
void Move::update()
{
   if(this->isValid() && !this->command)
   {
      /**
       * Notation: <from square><to square>[<promotion>][<capture>][<castling>]
       */
      this->getSrc().setCol(text[0]);
      this->getSrc().setRow(text[1]);
      this->getDest().setCol(text[2]);
      this->getDest().setRow(text[3]);

      if(text.length() == 5)
      {
         if(isPromote())
         {
            this->setPromoteTo(text[4]);
         }
         else if (isCapture())
         {
            this->setCapturePieceName(text[4]);
         }
      }

      if(text.length() == 6)
      {
         if (isCapture())
         {
            this->setCapturePieceName(text[5]);
         }
      }
   }
}

/*
 * Is the input a capture
 */
bool Move::isCapture()
{
   return this->capture;
}

/*
 * Is the input a command
 */
bool Move::isCommand()
{
   return this->command;
}

/*
 * Is the input a promote
 */
bool Move::isPromote()
{
   return this->promote;
}

/*
 * Is the input a castle command
 */
bool Move::isCastle()
{
   return (this->isCastleK() || this->isCastleQ());
}

/*
 * Are we castling the King?
 */
bool Move::isCastleK()
{
   return this->castleK;
}

/*
 * Are we castling the Queen?
 */
bool Move::isCastleQ()
{
   return this->castleQ;
}

/*
 * Does the En Passant
 */
bool Move::isEnpassant()
{
   return this->enpassant;
}

/*
 * Checks of the user input an en passant
 */
void Move::checkEnpassant(char & value)
{
   if (toupper(value) == 'E')
   {
      this->enpassant = true;
   }
}

/*
 * Checks if the user Castled
 */
void Move::checkCastle(char & value)
{
   if (value == 'c')
   {
      this->castleK = true;
   }

   if (value == 'C')
   {
      this->castleQ = true;
   }
}

/*
 * Sets the promotion of a given piece
 */
void Move::setPromoteTo(char & value)
{
   this->promoteTo = value;
}

/*
 * Gets the piece to be promoted
 */
char Move::getPromoteTo()
{
   return this->promoteTo;
}

/*
 * Gets the piece to be captured
 */
char Move::getCapturePieceName()
{
   return this->capturePieceName;
}

/*
 * Sets the piece to be captured
 */
void Move::setCapturePieceName(char & value)
{
   this->capturePieceName = value;
}

/**
 * Quick way to initialize or reset a move object incase it is being recycled.
 */
void Move::reset()
{
   this->command = false;
   this->castleK = false;
   this->castleQ = false;
   this->enpassant = false;
   this->promote = false;
   this->capture = false;
   this->capturePieceName = ' ';
   this->promoteTo = ' ';
}
