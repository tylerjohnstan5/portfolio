#ifndef PROCEDURAL_CHESS_MOVE_H
#define PROCEDURAL_CHESS_MOVE_H

#include <iostream>
#include <assert.h>
#include <stdio.h>
#include "Piece.h"
#include "Coordinate.h"

class Move
{
private:
   std::string text;
   char promoteTo;
   bool capture;
   char capturePieceName;
   Coordinate source;
   Coordinate dest;
   bool castleK;
   bool castleQ;
   bool promote;
   bool enpassant;
   bool command;
   bool validateInput(std::string input);
   bool isColIndicatorValid(char & value);
   bool isRowIndicatorValid(char & value);
   bool isCaptureIndicatorValid(char & value);
   bool isPromotionIndicatorValid(char & value, char & rowIndicator);
   bool isCastlingIndicatorValid(char & value);

   void checkEnpassant(char & value);
   void checkCastle(char & value);
   void setPromoteTo(char & value);
   void setCapturePieceName(char & value);
   void update();
   void reset();

public:
   Move();
   Coordinate & getSrc();
   Coordinate & getDest();
   std::string getText();
   bool isValid();
   bool isCapture();
   bool isCommand();
   bool isPromote();
   bool isCastle();
   bool isCastleK();
   bool isCastleQ();
   bool isEnpassant();

   // insertion and extraction operators
   friend std::ostream & operator << (std::ostream & out, Move & rhs);
   friend std::istream & operator >> (std::istream & in, Move & rhs);

   void setText(std::string value);
   void setText(Coordinate & src, Coordinate & dest);
   void setText(Coordinate & src, Coordinate & dest, Piece & toPiece);
   void setText(Coordinate & src, Coordinate & dest, Piece & toPiece, char promoteTo);
   char getPromoteTo();
   char getCapturePieceName();
};

#endif //PROCEDURAL_CHESS_MOVE_H
