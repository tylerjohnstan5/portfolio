//
// Created by Nicholas Dunnaway on 1/16/16.
//

#ifndef PROCEDURAL_CHESS_KNIGHT_H
#define PROCEDURAL_CHESS_KNIGHT_H


#include "Piece.h"

class Knight : public Piece {

public:
    Knight(bool isWhite);
};


#endif //PROCEDURAL_CHESS_KNIGHT_H
