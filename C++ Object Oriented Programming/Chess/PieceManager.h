#ifndef PROCEDURAL_CHESS_PIECEMANAGER_H
#define PROCEDURAL_CHESS_PIECEMANAGER_H

#include <iostream>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <vector>
#include "Board.h"

#define SPACE 1
#define WHITE 2
#define BLACK 3

/**
 * The PieceManager is responsible for enforcing that chess pieces move according to the chess rules.
 */
class PieceManager
{
private:
   enum Direction { North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest };
   Board & board;
   Move * lastMove;
   bool isValidMove(Move move);
   bool isValidMove(Pawn & piece, Move move);
   bool isValidMove(King & piece, Move move);
   bool isValidMove(Queen & piece, Move move);
   bool isValidMove(Bishop & piece, Move move);
   bool isValidMove(Knight & piece, Move move);
   bool isValidMove(Rook & piece, Move move);
   Direction getDirection(int colDistance, int rowDistance);
   int  checkLocation(int row, int col);
   std::vector <Move> getAvailableMoves(Pawn & piece, Coordinate coordinate);
   std::vector <Move> getAvailableMoves(King & piece, Coordinate coordinate);
   std::vector <Move> getAvailableMoves(Queen & piece, Coordinate coordinate);
   std::vector <Move> getAvailableMoves(Bishop & piece, Coordinate coordinate);
   std::vector <Move> getAvailableMoves(Knight & piece, Coordinate coordinate);
   std::vector <Move> getAvailableMoves(Rook & piece, Coordinate coordinate);
   Coordinate getCoordByOffSet(Coordinate src, int x_col, int y_row);
   bool addMove(std::vector<Move> * moves, Coordinate src, Coordinate dest);
   bool addMove(std::vector<Move> * moves, Move & move);
   bool isLocationOccupied(int col, int row);

public:
   PieceManager(Board & board);
   void updateBoard(Move move, bool isWhitesTurn);
   std::vector <Move> getAvailableMoves(Coordinate coordinate);


};

#endif //PROCEDURAL_CHESS_PIECEMANAGER_H
