#include "Game.h"

/*
 * Runs the game as long as end game = false
 */
Game::Game(Board & board, PieceManager & pieceManager) : board(board), pieceManager(pieceManager)
{
   endGame = false;
   assert(!endGame); //making sure it is false
}

/*
 * Runs the game
 */
void Game::run()
{
   while (!endGame)
   {
      std::cout << board;
      playGame();
   }
}

/**
 * Sets up how input is read
 * Notation: <from square><to square>[<capture>][<promotion>][<castling>]
 */
void Game::handleUserInput(Move move)
{
   if (move.isValid())
   {
      if (move.getText() == "?")
      {
         printHelp();
         playGame();
      }
      else if (move.getText() == "quit")
      {
         quit();
      }
      else if (move.getText() == "read")
      {
         readFromFile();
      }
      else if (move.getText() == "test")
      {
         board.toggleTestMode();
         std::cout << board;
         playGame();
      }
      else if (move.getText() == "help")
      {
         help();
      }
      else if (move.getText() == "rank")
      {
         rank();
      }
      else // Handle Valid Move.
      {
         // Update Board
         pieceManager.updateBoard(move, isWhiteTurn());

         // Record Move.
         moveList.push_back(move.getText());
      }
   }
   else
   {
      throw "Invalid Command";
   }
}

/*
 * displays the Help options
 */
void Game::printHelp()
{
   printMessage("Options:");
   printMessage(" ?      Display these options");
   printMessage(" b2b4   Specify a move using the Smith Notation");
   printMessage(" read   Read a saved game from a file");
   printMessage(" help   Display all possible moves for a given piece");
   printMessage(" test   Simple display for test purposes");
   printMessage(" rank   Who is winning?  What is the rank");
   printMessage(" quit   Leave the game. You will be prompted to save");
}

/**
 * Print a message to the screen.
 */
void Game::printMessage(std::string message)
{
   std::cout << message << std::endl;
}

/*
 * Gets the user input
 */
Move Game::getUserInput()
{
   Move input;
   std::cout << (isWhiteTurn() ? "(White): " : "(Black): ");
   std::cin >> input;
   return input;
}

/*
 * Quits the game
 */
void Game::quit()
{
   char str[256];
   endGame = true; // Kill main loop

   // Prompt ask user if they want to save.
   printMessage("To save a game, please specify the filename.");
   printMessage("    To quit without saving a file, just press <enter>");

   // Clear cin
   std::cin.clear();
   std::cin.ignore(256,'\n');
   std::cin.get(str, 256);

   if (charLength(str) == 0)
   {
      return;
   }
   writeToFile(str);
}

/*
 * Runs through the character lengths
 */
int Game::charLength(char * p1)
{
   char * p2 = p1;
   while (*(p2++))
      ;
   return (int) (p2 - p1 - 1);
}

/*
 * Writes the game to a file
 */
void Game::writeToFile(std::string fileName)
{
   std::ofstream fout(fileName.c_str());

   // ForEach move in the move list.
   for (int i = 0; i < moveList.size(); i++)
   {
      // Write the move to a file.
      if (i % 2 == 0)
      {
         fout << moveList[i];
      }
      else
      {
         fout << " " << moveList[i] << std::endl;
      }
   }

   if(fout.is_open())
   {
      fout.close();
   }
}

/**
 * Reads the game from a file
 */
void Game::readFromFile()
{
   std::string fileName;
   std::cout << "Filename: ";
   std::cin >> fileName;

   std::vector <std::string> fileMoveList;
   std::ifstream fin(fileName.c_str());

   if(fin.fail())
   {
      std::cout << "Unable to open file " << fileName << " for input." << std::endl;
      playGame();
      return;
   }

   std::string input;
   while(fin >> input)
   {
      fileMoveList.push_back(input);
   }

   if(fin.is_open())
   {
      fin.close();
   }

   Move currentMove;
   try
   {
      newGame();
      // ForEach move in the move list validate
      for (int i = 0; i < fileMoveList.size(); i++)
      {
         currentMove.setText(fileMoveList[i]);
         if(!currentMove.isValid()){
            break;
         }
      }

      // ForEach move in the move list.
      for (int i = 0; i < fileMoveList.size(); i++)
      {
         currentMove.setText(fileMoveList[i]);
         handleUserInput(currentMove);
      }
   }
   catch (const char * message)
   {
      std::cout << "Error parsing file " << fileName << " with move '" << currentMove << "': "  << message << std::endl;
      playGame();
   }
}

/*
 * Starts a new game
 */
void Game::newGame()
{
   board.newGame();
   moveList.clear();
}

/*
 * Allows us to play the game
 */
void Game::playGame()
{
   Move move;
   try
   {
      move = getUserInput();
      handleUserInput(move);
   }
   catch (const char * message)
   {
      std::cout << "Error in move '" << move << "': " << message << std::endl;
      printMessage("\tType ? for more options");
      playGame(); // recursion intended
   }
}

/*
 * Checks if its whites turn
 */
bool Game::isWhiteTurn()
{
   return (moveList.size() % 2 == 0);
}

/*
 * Displays all help text
 */
void Game::help()
{
   Coordinate coordinate;
   std::cout << "Which piece would you like to find the moves for? ";
   std::cin >> coordinate;
   if(!coordinate.isValid())
   {
      std::cout << "Error: Invalid specification of coordinates" << std::endl;
      return;
   }
   std::vector <Move> availableMoves = pieceManager.getAvailableMoves(coordinate);

   std::cout << "Possible moves are: " << std::endl;

   for (int i = 0; i < availableMoves.size(); i++ )
   {
      std::cout << '\t' << availableMoves[i] << std::endl;
   }
   playGame();
}

/**
 * Prints the space rank.
 */
void Game::rank()
{
   std::cout.setf(std::ios::fixed); // no scientific notation please
   std::cout.setf(std::ios::showpoint); // always show the decimal for real numbers
   std::cout.precision(2); // two digits after the decimal
   std::cout << "force: " << board.getRankForce() << std::endl;
   std::cout << "space: " << board.getRankSpace() << std::endl;
   std::cout << "time:  " << getTimingScore() << std::endl;
   playGame();
}

float Game::getTimingScore()
{
   float whiteScore = 0, blackScore = 0;
   for(int col = 0; col < 8; col++)
   {
      for(int row = 0; row < 8; row++)
      {
         Coordinate coordinate;
         coordinate.setCol(board.getColLabel(col));
         coordinate.setRow(board.getRowLabel(row));
         Piece * pPiece = board.getPiece(coordinate);

         if(!pPiece->isSpace())
         {
            std::vector <Move> availableMoves = pieceManager.getAvailableMoves(coordinate);

            if (pPiece->isWhite())
            {
               // add to whites score.
               whiteScore += availableMoves.size();
            }
            else
            {
               // Add to blacks score.
               blackScore += availableMoves.size();
            }
         }
      }
   }
   return (float) ((whiteScore * 0.2) - (blackScore * 0.2));
}
