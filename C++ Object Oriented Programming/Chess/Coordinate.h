#ifndef PROCEDURAL_CHESS_CORDS_H
#define PROCEDURAL_CHESS_CORDS_H

#include <iostream>

class Coordinate
{
private:
   char row;
   char col;
   bool fValid;
   void setValid(bool value);

public:
   Coordinate();
   void setRow(char value);
   void setCol(char value);
   char getRow();
   char getCol();
   bool isValid();

   // insertion and extraction operators
   friend std::ostream & operator << (std::ostream & out, Coordinate & rhs);
   friend std::istream & operator >> (std::istream & in, Coordinate & rhs);

};

#endif //PROCEDURAL_CHESS_CORDS_H
