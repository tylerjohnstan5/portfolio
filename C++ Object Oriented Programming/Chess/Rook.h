#ifndef PROCEDURAL_CHESS_ROOK_H
#define PROCEDURAL_CHESS_ROOK_H


#include "Piece.h"

class Rook : public Piece {

public:
   Rook(bool isWhite);
};


#endif //PROCEDURAL_CHESS_ROOK_H
