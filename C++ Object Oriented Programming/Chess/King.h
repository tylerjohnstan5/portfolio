//
// Created by Nicholas Dunnaway on 1/16/16.
//

#ifndef PROCEDURAL_CHESS_KING_H
#define PROCEDURAL_CHESS_KING_H


#include "Piece.h"

class King : public Piece {

public:
    King(bool isWhite);
};


#endif //PROCEDURAL_CHESS_KING_H
