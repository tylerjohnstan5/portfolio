#ifndef PROCEDURAL_CHESS_USERINPUTHANDLER_H
#define PROCEDURAL_CHESS_USERINPUTHANDLER_H
#include <iostream>
#include <vector>
#include <fstream>
#include <cassert>
#include "Board.h"
#include "PieceManager.h"
#include "Move.h"

class Game
{
private:
   std::vector <std::string> moveList;
   bool endGame;
   Board & board;
   PieceManager & pieceManager;
   void printMessage(std::string message);
   void printHelp();
   void readFromFile();
   void writeToFile(std::string fileName);
   Move getUserInput();
   void handleUserInput(Move move);
   void quit();
   int charLength(char * p1);
   void newGame();
   void playGame();
   bool isWhiteTurn();
   void help();
   void rank();
   float getTimingScore();

public:
   Game(Board & board, PieceManager & pieceManager);
   void run();
};


#endif //PROCEDURAL_CHESS_USERINPUTHANDLER_H
