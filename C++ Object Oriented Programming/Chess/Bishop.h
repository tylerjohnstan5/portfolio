//
// Created by Nicholas Dunnaway on 1/16/16.
//

#ifndef PROCEDURAL_CHESS_BISHOP_H
#define PROCEDURAL_CHESS_BISHOP_H


#include "Piece.h"

class Bishop : public Piece {

public:
    Bishop(bool isWhite);
};


#endif //PROCEDURAL_CHESS_BISHOP_H
