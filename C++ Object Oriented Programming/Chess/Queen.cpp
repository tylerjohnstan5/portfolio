//
// Creates the Queen piece to be used throughout the program
//

#include "Queen.h"

Queen::Queen(bool isWhite) : Piece(isWhite)
{
   setName('Q');
   setScore(9);
}
