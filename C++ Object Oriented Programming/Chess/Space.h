//
// Created by Nicholas Dunnaway on 1/16/16.
//

#ifndef PROCEDURAL_CHESS_SPACE_H
#define PROCEDURAL_CHESS_SPACE_H

#include "Piece.h"

class Space : public Piece
{

public:
    Space();
};


#endif //PROCEDURAL_CHESS_SPACE_H
