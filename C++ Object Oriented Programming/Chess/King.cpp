//
// Creates the King piece to be used throughout the program
//

#include "King.h"

King::King(bool isWhite) : Piece(isWhite)
{
   setName('K');
   setScore(0);
}
