//
// Created by Nicholas Dunnaway on 1/16/16.
//

#ifndef PROCEDURAL_CHESS_PAWN_H
#define PROCEDURAL_CHESS_PAWN_H


#include "Piece.h"

class Pawn : public Piece {

public:
    Pawn(bool isWhite);
};


#endif //PROCEDURAL_CHESS_PAWN_H
