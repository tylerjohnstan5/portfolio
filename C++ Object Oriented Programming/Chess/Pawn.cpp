//
// Creates the Pawn piece to be used throughout the program
//

#include "Pawn.h"

Pawn::Pawn(bool isWhite) : Piece(isWhite)
{
   setName('P');
   setScore(1);
}
