#ifndef mediumRock_h
#define mediumRock_h

#include "rock.h"
#include "uiDraw.h"
#include "point.h"
#include "smallRock.h"

class MediumRock : public Rock
{
public:
   MediumRock(float x, float y, float dx, float dy, bool isUp);
   void onHit();
   void draw();
   void advance();
};

#endif /* mediumRock_h */
