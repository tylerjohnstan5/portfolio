#include "level.h"

/*
 * initialize the level
 */
Level::Level(int levelNumber)
{
   for (int i = 0; i < (levelNumber + 1); i++)
   {
      this->scorableFlyingObjects.push_back(new LargeRock());
   }
}

std::vector<ScorableFlyingObject *> Level::getSpawnedScorableFlyingObjects()
{
   return this->scorableFlyingObjects;
}

/*
 * deconstruct the level
 */
Level::~Level()
{
   this->scorableFlyingObjects.clear();
}
