#ifndef ship_h
#define ship_h
#include "flyingObject.h"

#include "flyingObject.h"

#define INIT_ROTATION 0

// Put your Ship class here
class Ship : public FlyingObject
{
private:
   int respawnTimer;
   int shieldTimer;
   void reset();
   double degrees_to_radian(double deg);
   bool flame;

public:
   Ship();
   void warp();
   void thrust();
   void onHit();
   void advance();
   void draw();
   void onMoveRight(int pressedFrameCount);
   void onMoveLeft(int pressedFrameCount);
   bool isInLimbo();
   void resurrect();
   bool isShieldUp();
};

#endif /* ship_h */
