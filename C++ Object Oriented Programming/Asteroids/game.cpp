/*********************************************************************
 * File: game.cpp
 * Description: Contains the implementation of the game class
 *  methods.
 *
 *********************************************************************/

#include "game.h"

/*
 * initializes game
 */
Game::Game(Point pTopLeft, Point pBottomRight) : numberOfLives(NUMBER_OF_LIVES),
                                                 score(0),
                                                 topLeft(pTopLeft),
                                                 bottomRight(pBottomRight),
                                                 timer(0),
                                                 gameOver(false),
                                                 bulletCount(0),
                                                 gameLevel(1)
{
   handleNewLevel(); // Start at level one

   for(int i = 0; i < MAX_BULLETS; i++)
   {
      bullets[i] = new Bullet();
      bullets[i]->kill();
   }
}

/*
 * allows all objects and the game itself to advance
 * each frame
 */
void Game::advance()
{
   if(gameOver)
   {
      return;
   }

   timer++;
   if (timer < TITLE_SCREEN_TIMEOUT)
   {
      return;
   }

   // Move everything
   for(int i = 0; i < MAX_BULLETS; i++)
   {
      bullets[i]->advance();
   }

   std::list<ScorableFlyingObject *>::iterator it;
   for (it = scorableFlyingObjects.begin(); it != scorableFlyingObjects.end(); it++)
   {
      (* it)->advance();
   }

   this->ship.advance();

   // Check if anything hit each other.
   this->checkHits();
   this->handleSpawnedChildren();
   this->handleDeadScorableFlyingObjects();
   this->handleDeadShip();

   // end game if we run out of lives.
   if (numberOfLives < 0)
   {
      gameOver = true;
   }

   // deal with a new level
   if(this->scorableFlyingObjects.size() == 0)
   {
      gameLevel++;
      handleNewLevel();
   }

   // Spawn Saucer every 600 frames.
   if(timer % 600 == 0)
   {
      this->scorableFlyingObjects.push_back(new Saucer());
   }
}

/*
 * handles the input of the user to
 * interact with the game
 */
void Game::handleInput(const Interface pUI)
{
   // moved Q up so quit still works on game over.
   if (pUI.isQ())
   {
      onQ();
   }

   if (timer < TITLE_SCREEN_TIMEOUT || gameOver)
   {
      return;
   }

   if (pUI.isRight())
   {
      onMoveRight(pUI.isRight());
   }

   if (pUI.isLeft())
   {
      onMoveLeft(pUI.isLeft());
   }

   if (pUI.isUp())
   {
      onMoveUp(pUI.isUp());
   }

   if (pUI.isDown())
   {
      onMoveDown(pUI.isDown());
   }

   // use the space bar to shoot a bullet
   if (pUI.isSpace())
   {
      onSpace();
   }
}

// draw all the objects (ship. rocks, bullets...)
// calls the objects specific draw methid
void Game::draw(const Interface pUI)
{
   if(gameOver)
   {
      displayGameOverScreen();
      return;
   }

   if (timer < TITLE_SCREEN_TIMEOUT)
   {
      displayTitleScreen();
      return;
   }

   // draw bullets
   for(int i = 0; i < MAX_BULLETS; i++)
   {
      bullets[i]->draw();
   }

   // draw scorable stuff
   std::list <ScorableFlyingObject *> :: iterator it;
   for (it = scorableFlyingObjects.begin(); it != scorableFlyingObjects.end(); it++)
   {
      (* it)->draw();
   }

   // draw ship
   this->ship.draw();

   // draw score
   this->drawScore();

   // draw lives
   for (int i = numberOfLives, x = 10; i > 0; i--, x+= 15)
   {
      Point lives(topLeft.getX() + x, topLeft.getY() - 25);
      drawShip(lives, 0, false);
   }
}

// destructor for the game
Game::~Game()
{
   cleanUp();
}

// cleanup before exitting
void Game::cleanUp()
{
   for(int i = 0; i < MAX_BULLETS; i++)
   {
      delete bullets[i];
   }

   std::list <ScorableFlyingObject *> :: iterator it;
   for (it = scorableFlyingObjects.begin(); it != scorableFlyingObjects.end(); it++)
   {
      delete * it;
   }
}

/*
 * Quit the game
 */
void Game::onQ()
{
   cleanUp();
   exit(0); // We need to quit
}

// You may find this function helpful...

/**********************************************************
 * Function: getClosestDistance
 * Description: Determine how close these two objects will
 *   get in between the frames.
 **********************************************************/
float Game::getClosestDistance(FlyingObject * obj1, FlyingObject * obj2)
{
   // find the maximum distance traveled
   float dMax = (float) fmax(fabs((* obj1).getVelocity().getDx()), fabs((* obj1).getVelocity().getDy()));
   dMax = (float) fmax(dMax, fabs((* obj2).getVelocity().getDx()));
   dMax = (float) fmax(dMax, fabs((* obj2).getVelocity().getDy()));
   dMax = (float) fmax(dMax, 0.1f); // when dx and dy are 0.0. Go through the loop once.
   
   float distMin = std::numeric_limits<float>::max();
   for (float i = 0.0; i <= dMax; i++)
   {
      Point point1((* obj1).getPoint().getX() + ((* obj1).getVelocity().getDx() * i / dMax),
                   (* obj1).getPoint().getY() + ((* obj1).getVelocity().getDy() * i / dMax));
      Point point2((* obj2).getPoint().getX() + ((* obj2).getVelocity().getDx() * i / dMax),
                   (* obj2).getPoint().getY() + ((* obj2).getVelocity().getDy() * i / dMax));
      
      float xDiff = point1.getX() - point2.getX();
      float yDiff = point1.getY() - point2.getY();
      
      float distSquared = (xDiff * xDiff) +(yDiff * yDiff);
      
      distMin = (float) fmin(distMin, distSquared);
   }
   
   return (float) sqrt(distMin);
}

// conversion needed for getClosestDistance
double Game::degrees_to_radian(double deg)
{
   return deg * M_PI / 180.0;
}

// calculate where the bullet should start - X value
float Game::getBulletStartX(float angle, float hypotenuse)
{
   double returnValue = cos(degrees_to_radian(angle));
   returnValue = returnValue * hypotenuse;
   return (float) returnValue;
}

// calculate where the bullet should start - Y value
float Game::getBulletStartY(float angle, float hypotenuse)
{
   double returnValue = sin(degrees_to_radian(angle));
   returnValue = returnValue * hypotenuse;
   return (float) returnValue;
}

// drop into the ships rotation function
void Game::onMoveRight(int pressedFrameCount)
{
   this->ship
       .onMoveRight(pressedFrameCount);
}

// drop into the ships rotation function
void Game::onMoveLeft(int pressedFrameCount)
{
   this->ship
       .onMoveLeft(pressedFrameCount);
}

// drop into the ships thrust function
void Game::onMoveUp(int pressedFrameCount)
{
   this->ship.thrust();
}

// drop into the ships warp function
void Game::onMoveDown(int pressedFrameCount)
{
   this->ship.warp();
}

// fire a bullet
void Game::onSpace()
{
   if (ship.isDead() || ship.isInLimbo()) return;

   if(bulletCount < MAX_BULLETS)
   {
      // create a new bullet
      if(bullets[bulletCount]->isDead())
      {
         float x = getBulletStartX(ship.getRotation(), ship.getRadius());
         bullets[bulletCount]->getVelocity().setDx((float) (ship.getVelocity().getDx() - (5 * sin(degrees_to_radian(ship.getRotation())))));
         bullets[bulletCount]->getPoint().setX(ship.getPoint().getX());

         float y = getBulletStartY(ship.getRotation(), ship.getRadius());
         bullets[bulletCount]->getVelocity().setDy((float) (ship.getVelocity().getDy() + (5 * cos(degrees_to_radian(ship.getRotation())))));
         bullets[bulletCount]->getPoint().setY(ship.getPoint().getY());

         bullets[bulletCount]->resurrect();
         bulletCount++;
         if (bulletCount > (MAX_BULLETS - 1))
         {
            bulletCount = 0;
         }
      }
   }
}

// check to see if two objects hit each other
// looks for bullets hitting roctk and the saucer
// looks for the ship hitting rocks and the saucer
// handles changes in the scrore from hits
void Game::checkHits()
{
   // For each scorable object.
   std::list <ScorableFlyingObject *> :: iterator it;
   for (it = scorableFlyingObjects.begin(); it != scorableFlyingObjects.end(); it++)
   {
      // Check if the flying object is alive.
      if(!(* it)->isDead())
      {
         float hit;
         if (!ship.isDead())
         {
            // Did it hit the ship
            hit = (* it)->getRadius() + ship.getRadius();
            float dist = getClosestDistance((FlyingObject *) (* it), (FlyingObject *) & ship);
            if (dist <= hit)
            {
               addToScore((* it)->getScore());
               if (!ship.isShieldUp() && !ship.isInLimbo())
               {
                  numberOfLives--;
                  ship.onHit();
               }
               (* it)->onHit();
            }
         }

         // Check each bullet
         for (int i = 0; i < MAX_BULLETS; i++)
         {
            if (!bullets[i]->isDead())
            {
               hit = (* it)->getRadius();
               if (getClosestDistance((FlyingObject *) (* it), (FlyingObject *) bullets[i]) <= hit)
               {
                  addToScore((* it)->getScore());
                  bullets[i]->onHit();
                  (* it)->onHit();
               }
            }
         }
      }
   }
}

// adder for score
void Game::addToScore(int value)
{
   this->score += value;
}

// title screen display function
void Game::displayTitleScreen()
{
   Point title(-21, 75);
   drawText(title, "Asteroids");
   Point title2(-3, 50);
   drawText(title2, "By:");
   Point titleNick(-50, 25);
   drawText(titleNick, "Nicholas Dunnaway");
   Point titleFoster(-30, 0);
   drawText(titleFoster, "Foster Nuffer");
   Point titleTyler(-30, -25);
   drawText(titleTyler, "Tyler Stanley");
}

// game over display function
void Game::displayGameOverScreen()
{
   drawScore();
   Point title(-35, 0);
   drawText(title, "GAME OVER");
}

// handle spawned children - rocks
void Game::handleSpawnedChildren()
{
   std::list<ScorableFlyingObject *>::iterator it;
   for (it = scorableFlyingObjects.begin(); it != scorableFlyingObjects.end(); it++)
   {
      // Check if the object is dead.
      if ((* it)->isDead())
      {
         // Add any spawned children.
         for (unsigned long i = (* it)->getSpawnedChildren().size(); i > 0; i--)
         {
            this->scorableFlyingObjects.push_back((* it)->getSpawnedChildren()[i - 1]);
         }
      }
   }
}

// handle chanding the game level
void Game::handleNewLevel()
{
   Level newLevel(gameLevel);

   // Add any spawned flying objects.
   for (unsigned long i = newLevel.getSpawnedScorableFlyingObjects().size(); i > 0; i--)
   {
      this->scorableFlyingObjects.push_back(newLevel.getSpawnedScorableFlyingObjects()[i - 1]);
   }
}

// remove dead scoreable flying objects
void Game::handleDeadScorableFlyingObjects()
{
   scorableFlyingObjects.remove_if(is_Dead());
}

// ressurect a dead ship
void Game::handleDeadShip()
{
   if (!this->ship.isDead()) return;
   this->ship.resurrect();

}

// draw the score
void Game::drawScore()
{
   Point shotsHitPoint(topLeft.getX() + 5, topLeft.getY() - 5);
   drawNumber(shotsHitPoint, score);
}
