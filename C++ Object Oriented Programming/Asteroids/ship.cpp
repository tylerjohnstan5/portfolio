#include "ship.h"

/*
 * initializes the ship
 */
Ship::Ship() : respawnTimer(0), shieldTimer(0)
{
   reset();
   this->setRotation(INIT_ROTATION);
   this->getPoint().setWrap(true);
   this->setRadius(6);
}

/*
 * warps the ship
 */
void Ship::warp()
{
   if (isInLimbo() || isDead()) return;
   reset();
   getPoint().setX(random(-200,201));
   getPoint().setY(random(-200,201));
}

// accelerate the ship in the direction the
// ship is pointing
void Ship::thrust()
{
   if (isInLimbo() || isDead()) return;
   // increase the velocity in the same direction we are already going
   // look at the current ratio and increase it by the percentage
   // determine the direction of the thrust
   getVelocity().setDx((float) (getVelocity().getDx() - (.2 * sin(degrees_to_radian(getRotation())))));
   getVelocity().setDy((float) (getVelocity().getDy() + (.2 * cos(degrees_to_radian(getRotation())))));
   flame = true;
}

// converts degreees to radians
// in - double
// out double
double Ship::degrees_to_radian(double deg)
{
   return deg * M_PI / 180.0;
}

// handle what happens when something hits the ship
// used to kill the ship - but that moved to advance
// setting the resoawn timer iputs the ship in limbo
void Ship::onHit()
{
   if (isShieldUp()) return;
   this->respawnTimer = 150;
}

// handle moving the ship forward
void Ship::advance()
{

   if (respawnTimer != 0)
   {
      respawnTimer--;
      if (respawnTimer == 90)
      {
         kill();
         return;
      } 
   }

   if (isDead()) return;
   if (shieldTimer != 0)
   {
      shieldTimer--;
   }

   getPoint().addX(getVelocity().getDx());
   getPoint().addY(getVelocity().getDy());
}

// draw the ship
// if the ship is in limbo draw it flaming
void Ship::draw()
{
   if (isDead()) return;
   if (isInLimbo())
   {
      drawShipFire(this->getPoint(), getRotation(), true);
      return;
   }
   drawShip(this->getPoint(), getRotation(), flame);

   if (isShieldUp())
   {
      drawShipShield(this->getPoint(), 20);
   }
   flame = false;
}

// reset the ships location  to the center of the screen
// reset the velocity to 0
void Ship::reset()
{
   getPoint().setX(0);
   getPoint().setY(0);
   getVelocity().setDx(0);
   getVelocity().setDy(0);
}

// bring the ship back to life
// after its firey death!
void Ship::resurrect()
{
   if (respawnTimer > 0) return;
   this->shieldTimer = 180;
   this->reset();
   FlyingObject::resurrect();
}

// handle rotating the ship do to the right arrow key press
void Ship::onMoveRight(int pressedFrameCount)
{
   if (isInLimbo() || isDead()) return;
   setRotation(getRotation() - 10);
   if(getRotation() < MIN_ROTATION)
   {
      setRotation(MAX_ROTATION);
   }
}

// handle rotating the ship do to the left arrow key press
void Ship::onMoveLeft(int pressedFrameCount)
{
   if (isInLimbo() || isDead()) return;
   setRotation(getRotation() + 10);

   if(getRotation() > MAX_ROTATION)
   {
      setRotation(MIN_ROTATION);
   }
}

// determine if the ship is in limbo
// limbo is after the ship has been hit and before it
// is ressurected.
bool Ship::isInLimbo()
{ 
   return (respawnTimer > 90);
}

bool Ship::isShieldUp()
{
   return this->shieldTimer > 0;
}
