#ifndef VELOCITY_H
#define VELOCITY_H

class Velocity // Nick
{
private:
   float dx;
   float dy;

public:
   Velocity();
   float getDx();
   void setDx(float value);
   float getDy();
   void setDy(float value);
};

#endif /* velocity_h */
