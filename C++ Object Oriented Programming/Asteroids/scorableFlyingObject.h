#ifndef ScorableFlyingObject_h
#define ScorableFlyingObject_h

#include <vector>
#include "flyingObject.h"


class ScorableFlyingObject : public FlyingObject
{
private:
   int score;

protected:
   void setScore(int value);
   std::vector<ScorableFlyingObject *> children;

public:
   int getScore();
   std::vector<ScorableFlyingObject *> getSpawnedChildren();
   virtual void onHit() = 0;
   virtual void advance() = 0;
   virtual void draw() = 0;

};

#endif /* ScorableFlyingObject */
