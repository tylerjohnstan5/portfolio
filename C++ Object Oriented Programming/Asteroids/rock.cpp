#include "rock.h"

/*
 * Checks if the rock is dead if not it
 * advances
 */
void Rock::advance()
{
    if (this->isDead()) return;
    getPoint().addX(getVelocity().getDx());
    getPoint().addY(getVelocity().getDy());
}

/*
 * initializes the rock and allows each
 * rock to wrap around the board
 */
Rock::Rock(int spin, int size) : spin(spin), size(size)
{
    this->getPoint().setWrap(true);
}
