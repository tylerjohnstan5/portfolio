#ifndef largeRock_h
#define largeRock_h

#include "rock.h"
#include "uiDraw.h"
#include "point.h"
#include "smallRock.h"
#include "mediumRock.h"

class LargeRock : public Rock
{
public:
   LargeRock();
   void onHit();
   void draw();
   void advance();
};

#endif /* largeRock_h */
