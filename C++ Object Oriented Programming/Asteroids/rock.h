#ifndef rock_h
#define rock_h

#include "scorableFlyingObject.h"

#define LARGE_ROCK_SIZE 16
#define LARGE_ROCK_SPIN 2
#define MEDIUM_ROCK_SIZE 8
#define MEDIUM_ROCK_SPIN 5
#define SMALL_ROCK_SIZE 4
#define SMALL_ROCK_SPIN 10

#include "velocity.h"
#include "point.h"
#include "scorableFlyingObject.h"

// Put your rock class here
class Rock : public ScorableFlyingObject
{
protected:
   int spin;
   int size;

public:
   Rock(int spin, int size);
   virtual void onHit() = 0;
   virtual void advance();
   virtual void draw() = 0;
};

#endif /* rock_h */
