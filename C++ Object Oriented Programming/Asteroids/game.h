#ifndef GAME_H
#define GAME_H

/*********************************************************************
 * File: game.h
 * Description: Defines the game class for Asteroids
 *
 *********************************************************************/

#include <limits>
#include <algorithm>
#include <list>
#include <cmath>
#include "bullet.h"
#include "scorableFlyingObject.h"
#include "ship.h"
#include "point.h"
#include "uiInteract.h"
#include "uiDraw.h"
#include "flyingObject.h"
#include "largeRock.h"
#include "saucer.h"
#include "level.h"

const int MAX_BULLETS = 20;
const int NUMBER_OF_LIVES = 4;
const int TITLE_SCREEN_TIMEOUT = 50;
const int windowSize = 200;
const int MAX_X = windowSize;
const int MAX_Y = windowSize;
const int MIN_X = windowSize * -1;
const int MIN_Y = windowSize * -1;

struct is_Dead {
   bool operator() (ScorableFlyingObject * value) { return value->isDead(); }
};

class Game // Nick
{
private:
   int timer;
   int numberOfLives;
   int score;
   int gameLevel;
   bool gameOver;
   std::list <ScorableFlyingObject *> scorableFlyingObjects;
   Bullet * bullets[MAX_BULLETS];
   int bulletCount;
   Ship ship;
   Point topLeft;
   Point bottomRight;
   double degrees_to_radian(double deg);
   float getBulletStartX(float angle, float hypotenuse);
   float getBulletStartY(float angle, float hypotenuse);
   void cleanUp();
   void onMoveRight(int pressedFrameCount);
   void onMoveLeft(int pressedFrameCount);
   void onMoveUp(int pressedFrameCount);
   void onMoveDown(int pressedFrameCount);
   void onSpace();
   void onQ();
   float getClosestDistance(FlyingObject * obj1, FlyingObject * obj2);
   void checkHits();
   void addToScore(int value);
   void displayTitleScreen();
   void displayGameOverScreen();
   void handleSpawnedChildren();
   void handleDeadScorableFlyingObjects();
   void handleDeadShip();
   void drawScore();
   void handleNewLevel();

public:
   Game(Point topLeft, Point bottomRight);
   ~Game();
   void advance();
   void handleInput(const Interface pUI);
   void draw(const Interface pUI);
};

#endif /* GAME_H */
