#ifndef bullet_h
#define bullet_h
#include "flyingObject.h"

#include "flyingObject.h"

#define BULLET_SPEED 5
#define BULLET_LIFE 40
#include "uiDraw.h"
#include "flyingObject.h"

// Put your Bullet class here
class Bullet : public FlyingObject
{
private:
   int lifeTimer;

public:
   Bullet();
   void onHit();
   void advance();
   void draw();
   void resurrect();
};

#endif /* bullet_h */
