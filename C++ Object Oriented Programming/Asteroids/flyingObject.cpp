#include "flyingObject.h"

/*
 * gets the velocity
 */
Velocity & FlyingObject::getVelocity()
{
   return this->velocity;
}

/*
 * sets the velocity
 */
void FlyingObject::setVelocity(Velocity value)
{
   this->velocity = value;
}

/*
 * gets the point used to identify
 * where an object is in the game
 */
Point & FlyingObject::getPoint()
{
   return this->point;
}

/*
 * sets the point
 */
void FlyingObject::setPoint(Point value)
{
   this->point = value;
}

/*
 * gets the rotation
 */
int FlyingObject::getRotation()
{
   return this->rotation;
}

/*
 * sets the rotation
 */
void FlyingObject::setRotation(int value)
{
   this->rotation = value;
}

/*
 * checks if the flying object is dead
 */
bool FlyingObject::isDead()
{
   return this->point.isDead();
}

/*
 * kills the flying object
 */
void FlyingObject::kill()
{
   this->point.kill();
}

/*
 * resurrects the flying object
 */
void FlyingObject::resurrect()
{
   this->point.resurrect();
}

/*
 * gets the radius of the flying object
 */
float FlyingObject::getRadius()
{
   return this->radius;
}

/*
 * sets the radius of the flying object
 */
void FlyingObject::setRadius(float value)
{
   this->radius = value;
}
