#include "saucer.h"

/*
 * initializes the saucer
 */
Saucer::Saucer() : lifeTimer(SAUCER_LIFE)
{
   // get a random location for the Saucer
   // it should be 15 points away from the center of the field
   // note: (random(1, 2) == 2 ? 1 : -1) gives a random 1 or -1
   getPoint().setX(random(50,181)  * (random(1, 3) == 2 ? 1 : -1));
   getPoint().setY(random(50,181)  * (random(1, 3) == 2 ? 1 : -1));
   getVelocity().setDx(random(1,4) * (random(1, 3) == 2 ? 1 : -1));
   getVelocity().setDy(random(1,4) * (random(1, 3) == 2 ? 1 : -1));
   getPoint().setWrap(true);
   setScore(500);
   setRadius(6);
}

/**********************************************
* void onHit
* kill the saucer
***********************************************/
void Saucer::onHit()
{
   kill();
}

// move the saucer by changing its location
void Saucer::advance()
{
   std::cout << lifeTimer << std::endl;
   lifeTimer--;
   getPoint().addX(getVelocity().getDx());
   getPoint().addY(getVelocity().getDy());
   if (lifeTimer <= 0)
   {
      kill();
   }
}

// draw the saucer at the current 'location'
void Saucer::draw()
{
   if (!isDead())
   {
      drawSaucer(getPoint());
   }
}
