#include "mediumRock.h"

/*
 * initialize the medium rock
 */
MediumRock::MediumRock(float x, float y, float dx, float dy, bool isUp) : Rock(MEDIUM_ROCK_SPIN, MEDIUM_ROCK_SIZE)
{
    getPoint().setX(x);
    getPoint().setY(y);
    getVelocity().setDx(dx);
    getVelocity().setDy((dy + (isUp ? 2 : -1)));
    setScore(2);
    this->setRadius(15);
    this->setRotation(5);
}

/*
 * if the medium rock is hit kill it
 * and draw 2 small rocks, one moving left the other right.
 */
void MediumRock::onHit()
{
    kill();
    children.push_back(new SmallRock(getPoint().getX(), getPoint().getY(), getVelocity().getDx(),getVelocity().getDy(), true));
    children.push_back(new SmallRock(getPoint().getX(), getPoint().getY(), getVelocity().getDx(),getVelocity().getDy(), false));
}

/*
 * draws the medium rock based on where the large rock
 * was killed
 */
void MediumRock::draw()
{
    drawMediumAsteroid(getPoint(), getRotation());
}

/*
 * calls the advance function that allows
 * the medium rock to move. Also sets rotation.
 */
void MediumRock::advance()
{
    Rock::advance();
    this->setRotation(this->getRotation() + 5);
}
