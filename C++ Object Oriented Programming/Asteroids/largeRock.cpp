#include "largeRock.h"

/*
 * initialize the large rock
 */
LargeRock ::LargeRock() : Rock(LARGE_ROCK_SPIN, LARGE_ROCK_SIZE)
{
   // get a random location for the astroid
   // it should be 15 points away from the center of the field
   // note: (random(1, 2) == 2 ? 1 : -1) gives a random 1 or -1
   getPoint().setX(random(15,201) * (random(1, 3) == 2 ? 1 : -1));
   getPoint().setY(random(15,201) * (random(1, 3) == 2 ? 1 : -1));

   // get a random velocity
   getVelocity().setDx((1 * (random(1, 3) == 2 ? 1 : -1)));
   getVelocity().setDy((1 * (random(1, 3) == 2 ? 1 : -1)));
   setScore(1);
   this->setRadius(20);
   this->setRotation(2);
}

/*
 * on Hit kill the large rock and spawn 2 medium rocks
 * one going up the other down. also spawn 1 small rock
 * going right
 */
void LargeRock::onHit()
{
   kill();
   children.push_back(new SmallRock( getPoint().getX(), getPoint().getY(), getVelocity().getDx(), getVelocity().getDy(), true));
   children.push_back(new MediumRock(getPoint().getX(), getPoint().getY(), getVelocity().getDx(), getVelocity().getDy(), true));
   children.push_back(new MediumRock(getPoint().getX(), getPoint().getY(), getVelocity().getDx(), getVelocity().getDy(), false));
}

/*
 * draws the large rock
 */
void LargeRock::draw()
{
   if(!isDead())
   {
      drawLargeAsteroid(getPoint(), getRotation());
   }
}

/*
 * calls the advance function that moves the large rock
 */
void LargeRock::advance()
{
    Rock::advance();
    this->setRotation(this->getRotation() + 2);
}
