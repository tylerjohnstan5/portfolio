#ifndef smallRock_h
#define smallRock_h

#include "rock.h"
#include "uiDraw.h"

class SmallRock : public Rock
{
public:
   SmallRock(float x, float y, float dx, float dy, bool isRight);
   void onHit();
   void draw();
   void advance();
};

#endif /* smallRock_h */
