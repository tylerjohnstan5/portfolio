#include "velocity.h"

/**
 * ctor
 */
Velocity::Velocity() : dx(0), dy(0)
{

}

float Velocity::getDx()
{
   return this->dx;
}

void Velocity::setDx(float value)
{
   this->dx = value;
}

float Velocity::getDy()
{
   return dy;
}

void Velocity::setDy(float value)
{
   this->dy = value;
}
