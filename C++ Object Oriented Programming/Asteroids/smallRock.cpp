#include "smallRock.h"

/*
 * initialize the small rock
 */
SmallRock :: SmallRock(float x, float y, float dx, float dy, bool isRight) : Rock(SMALL_ROCK_SPIN,SMALL_ROCK_SIZE)
{
   getPoint().setX(x);
   getPoint().setY(y);
   getVelocity().setDx((dx + (isRight ? 3 : -1)));
   getVelocity().setDy(dy);
   setScore(3);
   this->setRadius(6);
}

/*
 * if the small rock is hit kill it.
 */
void SmallRock::onHit()
{
   kill();
}

/*
 * draws the small asteroid based on the where a medium or large rock
 * was last destroyed
 */
void SmallRock::draw()
{
   drawSmallAsteroid(getPoint(), getRotation());
}

/*
 * Calls the advance function allowing the small rock to move
 * also sets the rotation
 */
void SmallRock::advance()
{
   Rock::advance();
   this->setRotation(this->getRotation() + 10);
}
