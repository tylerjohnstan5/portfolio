###############################################################
# Program:
#     Project 4, Asteroids
#     Brother McCracken, CS165
#
# Author:
#    (35%) Foster Nuffer - fosternuffer@gmail.com
#    (49.75%) Nicholas Dunnaway - nick@dunnaway.org
#    (15.25%) Tyler Stanley - sta09004@byui.edu
#
# Summary:
#     Introduced by Atari in 1979, Asteroids was one of the most
#     successful of the first generation of arcade games and was
#     highly influential on the video game landscape for the next
#     decade. The basic premise of the game is that a ship moves
#     around the screen shooting asteroids while trying not to
#     get hit by them.
#
# Above and Beyond
#     Press Q key to quit the game.
#     Press the down arrow to warp the ship. (Ship warp drive is unlimited, please abuse!)
#     There is a game score and each object has a point value.
#     There are a five lives.
#     Remaining lives are displayed as ships below the score.
#     Every n'th frame an enemy saucer spawns and it worth 500 points.
#     Enemy saucer only lives for short window.
#     On re-spawn ship has a shield for six secs that makes it invincible.
#     Title Screen
#     When all lives are lost game ends.
#     GAME OVER screen.
#     On hit, the ship catches fire, control is lost and then dies after two secs.
#     Added levels where after all objects are cleared from the screen a new level starts with more flying objects.
###############################################################

OBJS = main.o game.o uiInteract.o uiDraw.o point.o velocity.o flyingObject.o ship.o saucer.o bullet.o rock.o scorableFlyingObject.o mediumRock.o largeRock.o smallRock.o level.o
CC = g++
FLAGS = -std=c++0x
CFLAGS = -std=c++0x -c
LFLAGS = -lglut -lGLU -lGL

###############################################################
# Build the main game
###############################################################
a.out :
	echo done

asteroids : $(OBJS)
	$(CC) $(FLAGS) -o asteroids $(OBJS) $(LFLAGS)

###############################################################
# Individual files
#    uiDraw.o       Draw polygons on the screen and do all OpenGL graphics
#    uiInteract.o   Handles input events
#    point.o        The position on the screen
#    game.o         Handles the game interaction
#    velocity.o     Velocity (speed and direction)
#    flyingObject.o Base class for all flying objects
#    ship.o         The player's ship
#    bullet.o       The bullets fired from the ship
#    rocks.o        Contains all of the Rock classes
###############################################################
uiDraw.o: uiDraw.cpp uiDraw.h
	$(CC) $(CFLAGS) uiDraw.cpp

uiInteract.o: uiInteract.cpp uiInteract.h
	$(CC) $(CFLAGS) uiInteract.cpp

point.o: point.cpp point.h
	$(CC) $(CFLAGS) point.cpp

velocity.o: velocity.cpp velocity.h
	$(CC) $(CFLAGS) velocity.cpp

level.o: level.cpp level.h scorableFlyingObject.h largeRock.h
	$(CC) $(CFLAGS) level.cpp

flyingObject.o: flyingObject.cpp flyingObject.h point.h velocity.h uiDraw.h
	$(CC) $(CFLAGS) flyingObject.cpp

ship.o: ship.cpp ship.h flyingObject.h point.h velocity.h uiDraw.h
	$(CC) $(CFLAGS) ship.cpp

saucer.o: saucer.cpp saucer.h scorableFlyingObject.h point.h velocity.h uiDraw.h
	$(CC) $(CFLAGS) saucer.cpp

bullet.o: bullet.cpp bullet.h flyingObject.h point.h velocity.h uiDraw.h
	$(CC) $(CFLAGS) bullet.cpp

scorableFlyingObject.o: scorableFlyingObject.cpp scorableFlyingObject.h point.h velocity.h uiDraw.h flyingObject.h
	$(CC) $(CFLAGS) scorableFlyingObject.cpp

rock.o: rock.cpp rock.h flyingObject.h point.h velocity.h uiDraw.h
	$(CC) $(CFLAGS) rock.cpp

largeRock.o: largeRock.cpp largeRock.h rock.cpp rock.h flyingObject.h scorableFlyingObject.h point.h velocity.h uiDraw.h
	$(CC) $(CFLAGS) largeRock.cpp

mediumRock.o: mediumRock.cpp mediumRock.h rock.cpp rock.h flyingObject.h scorableFlyingObject.h point.h velocity.h uiDraw.h
	$(CC) $(CFLAGS) mediumRock.cpp

smallRock.o: smallRock.cpp smallRock.h rock.cpp rock.h flyingObject.h scorableFlyingObject.h point.h velocity.h uiDraw.h
	$(CC) $(CFLAGS) smallRock.cpp

game.o: game.cpp game.h uiDraw.h uiInteract.h point.h velocity.h flyingObject.h scorableFlyingObject.h bullet.h ship.h saucer.h level.h
	$(CC) $(CFLAGS) game.cpp

main.o: main.cpp game.h
	$(CC) $(CFLAGS) main.cpp

###############################################################
# General rules
###############################################################
clean:
	rm a.out *.o asteroids

tar :
	tar -cf project4.tar *.h *.cpp makefile

update :
	git fetch
	git pull
