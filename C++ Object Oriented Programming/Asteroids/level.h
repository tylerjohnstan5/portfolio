#ifndef CS165_ASTEROIDS_LEVEL_H
#define CS165_ASTEROIDS_LEVEL_H

#include <vector>
#include "scorableFlyingObject.h"
#include "largeRock.h"

class Level
{
private:
   std::vector<ScorableFlyingObject *> scorableFlyingObjects;

public:
   Level(int levelNumber);
   ~Level();
   std::vector<ScorableFlyingObject *> getSpawnedScorableFlyingObjects();
};


#endif //CS165_ASTEROIDS_LEVEL_H
