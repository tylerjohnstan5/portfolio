#include "scorableFlyingObject.h"

/*
 * gets the score of the object
 */
int ScorableFlyingObject::getScore()
{
 return this->score;
}

/*
 * sets the score of the object
 */
void ScorableFlyingObject::setScore(int value)
{
 this->score = value;
}

/*
 * puts the scorableFlyingObject in a vector
 */
std::vector<ScorableFlyingObject *> ScorableFlyingObject::getSpawnedChildren()
{
 return this->children;
}
