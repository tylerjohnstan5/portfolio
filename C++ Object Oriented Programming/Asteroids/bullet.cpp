#include "bullet.h"

/*
 * initializes the bullet and allows it to wrap around
 */
Bullet::Bullet() : lifeTimer(BULLET_LIFE)
{
   this->getPoint().setWrap(true);
}

/*
 * if the bullet hits an object kill it
 */
void Bullet::onHit()
{
   if (isDead()) return;
   kill();
}

/*
 * if the bullet is alive advance the bullet until
 * its life runs out
 */
void Bullet::advance()
{
   if (isDead()) return;
   lifeTimer--;
   getPoint().addX(getVelocity().getDx());
   getPoint().addY(getVelocity().getDy());

   if (lifeTimer <= 0)
   {
      kill();
   }
}

/*
 * draws the bullet
 */
void Bullet::draw()
{
   if (isDead()) return;

   if(!this->isDead())
   {
      drawDot(this->getPoint());
   }
}

/*
 * resurrects the bullet once it hits an object
 * or the life timer expires
 */
void Bullet::resurrect()
{
   FlyingObject::resurrect();
   lifeTimer = BULLET_LIFE;
}
