#ifndef flyingObject_h
#define flyingObject_h

#include "velocity.h"
#include "point.h"
#include "uiDraw.h"

#define MIN_ROTATION 0
#define MAX_ROTATION 359

class FlyingObject // Nick
{

private:
   Velocity velocity;
   Point point;
   float radius;
   int rotation;
   void setVelocity(Velocity value);
   void setPoint(Point value);

public:
   Velocity & getVelocity();
   Point & getPoint();
   float getRadius();
   void setRadius(float value);
   int getRotation();
   void setRotation(int value);
   bool isDead();
   void kill();
   virtual void resurrect();
   virtual void advance() = 0;
   virtual void draw() = 0;
   virtual void onHit() = 0;
};

#endif /* flyingObject_h */
