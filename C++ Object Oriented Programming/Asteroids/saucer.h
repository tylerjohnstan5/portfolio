#ifndef saucer_h
#define saucer_h
#include "scorableFlyingObject.h"

#define SAUCER_LIFE 360

// Put your Saucer class here
class Saucer : public ScorableFlyingObject
{
private:
   int lifeTimer;

public:
   Saucer();
   void onHit();
   void advance();
   void draw();
};

#endif /* saucer_h */
