#ifndef CS165_SKEET_PIGEON_H
#define CS165_SKEET_PIGEON_H

#include "point.h"

#include "uiDraw.h"

class Pigeon
{
private:
   const int MAX_RADIUS;
   const int MIN_RADIUS;
   const int DEFAULT_RADIUS;
   Point location;
   float velocity_x;
   float velocity_y;
   int radius;
   bool varRadius; // make the pigeon small as it travels
   void setShrinkingRadius();

public:
   Pigeon();

   void draw();

   void advance();

   void kill();

   void resurrect();

   bool isDead();

   float getX();

   float getY();

   void setX(float x);

   void setY(float y);

   void setVelocityX(float x);

   void setVelocityY(float y);

   float getVelocityX() { return velocity_x; }

   float getVelocityY() { return velocity_y; }

   void setRadius(int newRadius);

   int getRadius();

   void toggleShrink();

};

#endif //CS165_SKEET_PIGEON_H
