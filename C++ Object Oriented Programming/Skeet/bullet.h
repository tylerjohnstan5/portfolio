#ifndef CS165_SKEET_BULLET_H
#define CS165_SKEET_BULLET_H

#include "point.h"
#include "uiDraw.h"

class Bullet
{
private:
   Point location;
   float velocity_x;
   float velocity_y;

public:
   Bullet(int x, int y);
   void draw();
   void advance();
   void kill();
   void resurrect();
   bool isDead();
   float getX();
   float getY();
   void setX(float x);
   void setY(float y);
   void setVelocityX(float x);
   void setVelocityY(float y);
   float getVelocityX() { return velocity_x;}
   float getVelocityY() { return velocity_y;}

};

#endif //CS165_SKEET_BULLET_H
