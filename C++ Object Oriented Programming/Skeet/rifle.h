#ifndef CS165_SKEET_GUN_H
#define CS165_SKEET_GUN_H

#include "point.h"
#include "uiDraw.h"

class Rifle
{
private:
   Point location;
   unsigned int angle;
   unsigned int angleMin;
   unsigned int angleMax;
   int width;
   int height;

public:
   Rifle();
   unsigned int getAngle();
   void draw();
   void onMoveRight(int pressedFrameCount);
   void onMoveLeft(int pressedFrameCount);
   int getWidth();

};

#endif //CS165_SKEET_GUN_H
