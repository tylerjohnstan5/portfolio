#include "uiInteract.h"
#include "skeet.h"

void callBack(const Interface *pUI, void * p);

// set the bounds of the drawing rectangle
float Point::xMin = MIN_X;
float Point::xMax = MAX_X;
float Point::yMin = MIN_Y;
float Point::yMax = MAX_Y;

/*********************************
 * Main is pretty sparse.  Just initialize
 * my ball type and call the display engine.
 * That is all!
 *********************************/
int main(int argc, char ** argv)
{
   Interface ui(argc, argv, "Skeet");    // initialize OpenGL
   Skeet skeet;                          // initialize the game state
   ui.run(callBack, &skeet);             // set everything into action

   return 0;
}

/*************************************
 * All the interesting work happens here, when
 * I get called back from OpenGL to draw a frame.
 * When I am finished drawing, then the graphics
 * engine will wait until the proper amount of
 * time has passed and put the drawing on the screen.
 **************************************/
void callBack(const Interface *pUI, void * p)
{
   Skeet * pSkeet = (Skeet *)p;  // cast the void pointer into a known type

   if (pUI->isRight())
   {
      pSkeet->onMoveRight(pUI->isRight());
   }

   if (pUI->isLeft())
   {
      pSkeet->onMoveMoveLeft(pUI->isLeft());
   }

   // use the space bar to shoot a bullet
   if (pUI->isSpace())
   {
      pSkeet->onSpace();
   }

   if (pUI->isQ())
   {
      pSkeet->onQ();
   }

   if (pUI->isP())
   {
      pSkeet->onP();
   }

   pSkeet->draw();

}
