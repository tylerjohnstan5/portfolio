#ifndef CS165_SKEET_SKEET_H
#define CS165_SKEET_SKEET_H

#include <cmath>   // for M_PI, sin() and cos()
#include <cstdlib> // for Quit
#include "point.h"
#include "uiInteract.h"
#include "uiDraw.h"
#include "rifle.h"
#include "bullet.h"
#include "pigeon.h"

const int MAX_BULLETS = 5;
const int windowSize = 128;
const int MAX_X = windowSize;
const int MAX_Y = windowSize;
const int MIN_X = windowSize * -1;
const int MIN_Y = windowSize * -1;

class Skeet
{
private:
   unsigned int shotsHit;
   unsigned int shotsMissed;
   Rifle rifle;
   Pigeon pigeon;
   unsigned int getXOffset();
   Bullet * bullets[MAX_BULLETS];
   int bulletCount;
   double degrees_to_radian(double deg);
   void cleanUp();

public:
   Skeet();
   ~Skeet();
   unsigned int getShotsHit();
   unsigned int getShotsMissed();
   void draw();
   void onMoveRight(int pressedFrameCount);
   void onMoveMoveLeft(int pressedFrameCount);
   void onSpace();
   void onQ();
   void onP();
   void displayTitleScreen();
   float getBulletStartX(unsigned int angle, int hypotenuse);
   float getBulletStartY(unsigned int angle, int hypotenuse);
   void strike();                   // did we hit something?
   void incScore() { shotsHit++;}
   void incShotsMissed() { shotsMissed++;}
   float minDistance(Bullet *bullet, Pigeon & pigeon);

};


#endif //CS165_SKEET_SKEET_H
