#include "pigeon.h"
#include "skeet.h"

Pigeon::Pigeon() : MAX_RADIUS(20), MIN_RADIUS(5), DEFAULT_RADIUS(10)
{
   velocity_x = random(3, 6);
   location.setX(MIN_X);
   location.setY(random(MIN_Y, MAX_Y));
   if (getY() > 0)
   {
      velocity_y = random(-4, -1);
   }
   else
   {
      velocity_y = random(0, 4);
   }
   radius = DEFAULT_RADIUS;
   varRadius = false;
   location.setCheck(true);
   kill();
}

void Pigeon::draw()
{
   if (!location.isDead())
   {
      Point pigeon(location.getX(), location.getY());
      setShrinkingRadius();
      drawCircle(location, radius);
   }
}

void Pigeon::advance()
{
   location.addX(velocity_x);
   location.addY(velocity_y);
}

void Pigeon::kill()
{
   location.kill();
}

bool Pigeon::isDead()
{
   return location.isDead();
}

void Pigeon::setX(float x)
{
   location.setX(x);
}

void Pigeon::setY(float y)
{
   location.setY(y);
}

void Pigeon::resurrect()
{
   setX(MIN_X);
   setY(random(MIN_Y, MAX_Y));
   if (getY() > 0)
   {
      setVelocityY(random(-4, -1));
   }
   else
   {
      setVelocityY(random(0, 4));
   }
   setVelocityX(random(3, 6));
   location.resurrect();
}

void Pigeon::setVelocityX(float x)
{
   velocity_x = x;
}

void Pigeon::setVelocityY(float y)
{
   velocity_y = y;
}

float Pigeon::getX()
{
   return location.getX();
}

float Pigeon::getY()
{
   return location.getY();
}

void Pigeon::setRadius(int newRadius)
{
   radius = newRadius;
}

int Pigeon::getRadius()
{
   return radius;
}

void Pigeon::setShrinkingRadius()
{
   if (varRadius)
   {
      radius = (int) (MAX_RADIUS - (MAX_RADIUS - MIN_RADIUS) * ((location.getX() + MAX_X) / (MAX_X * 2)));
   }
}

void Pigeon::toggleShrink()
{
   varRadius = !varRadius;

   if (!varRadius)
      radius = DEFAULT_RADIUS;
}
