#include "skeet.h"

Skeet::Skeet()
{
   shotsHit = 0;
   shotsMissed = 0;
   bulletCount = 0;

   for(int i = 0; i < MAX_BULLETS; i++)
   {
      bullets[i] = new Bullet(MAX_X, MIN_Y);
      bullets[i]->kill();
   }
}

unsigned int Skeet::getShotsHit()
{
   return this->shotsHit;
}

unsigned int Skeet::getShotsMissed()
{
   return this->shotsMissed;
}

/*****************************************
 * draw all the items on the screen
 * includes 
 *   drawing the score
 *   drawing the bullets
 *   drawing the rifle
 *   drawing the pigeon
 *      checking for a hit
 ****************************************/
void Skeet::draw()
{
   static int timer = 0;
   static int delay = random(20,30);
   
   if (timer < 150)
   {
      displayTitleScreen();
      timer++;
      return;
   }

   // draw the score
   Point shotsHitPoint(MIN_X + 5, MAX_Y - 5);
   drawNumber(shotsHitPoint, getShotsHit());

   Point shotsMissedPoint(MAX_X - getXOffset(), MAX_Y - 5);
   drawNumber(shotsMissedPoint, getShotsMissed());

   // draw the bullets
   for(int i = 0; i < MAX_BULLETS; i++)
   {
      if(!bullets[i]->isDead())
      {
         bullets[i]->advance();
         bullets[i]->draw();
      }
   }
   
   // draw the riffle
   rifle.draw();

   // draw the pigeon
   if (!pigeon.isDead())
   {
      pigeon.advance();
      pigeon.draw();
      strike();
   }
   else
   {
       if (delay == 0)
    	 {
          // check to see if the pigeon went off the screen (right side, top, bottom)
          if ((pigeon.getX() >= MAX_X-2) ||
              (pigeon.getY() >= MAX_Y-2) || 
              (pigeon.getY() <= MIN_Y+2))
          {
              // pigeon went off the screen
              incShotsMissed(); // increment the missed score
          }
           pigeon.resurrect();
           delay = random(0,30);
    	 }
       else
	     {
	       delay--;
	     }
   }
}


/**
 * Gives the offset needed to right align the score.
 */
unsigned int Skeet::getXOffset()
{
   unsigned int xOffset = 11;
   int missed = getShotsMissed();

   if(missed > 9)
   {
      xOffset += 10;
   }

   if(missed > 99)
   {
      xOffset += 10;
   }

   if(missed > 999)
   {
      xOffset += 11;
   }

   return xOffset;
}

void Skeet::onMoveRight(int pressedFrameCount)
{
   this->rifle
       .onMoveRight(pressedFrameCount);
}

void Skeet::onMoveMoveLeft(int pressedFrameCount)
{
   this->rifle
       .onMoveLeft(pressedFrameCount);
}

void Skeet::onP()
{
   pigeon.toggleShrink();
}

void Skeet::onSpace()
{
   if(bulletCount < MAX_BULLETS)
   {
      // create a new bullet
      if(bullets[bulletCount]->isDead())
      {
         float x = getBulletStartX(rifle.getAngle(), rifle.getWidth()/2);
         bullets[bulletCount]->setVelocityX(x);
         x += MAX_X;
         bullets[bulletCount]->setX(x-1); // Offset by one so 90 degree shots appear on screen

         float y = getBulletStartY(rifle.getAngle(), rifle.getWidth()/2);
         bullets[bulletCount]->setVelocityY(y);
         y += MIN_Y;
         bullets[bulletCount]->setY(y);

         bullets[bulletCount]->resurrect();
         bulletCount++;
         if (bulletCount > (MAX_BULLETS - 1))
         {
            bulletCount = 0;
         }
      }
   }
}

void Skeet::displayTitleScreen()
{
   Point title(-12, 75);
   drawText(title, "Skeet");
   Point title2(-3, 50);
   drawText(title2, "By:");
   Point titleNick(-50, 25);
   drawText(titleNick, "Nicholas Dunnaway");
   Point titleFoster(-30, 0);
   drawText(titleFoster, "Foster Nuffer");
   Point titleTyler(-30, -25);
   drawText(titleTyler, "Tyler Stanley");
}


float Skeet::getBulletStartX(unsigned int angle, int hypotenuse)
{
   double returnValue = cos(degrees_to_radian(angle));
   returnValue = returnValue * hypotenuse;
   return (float) returnValue;
}

float Skeet::getBulletStartY(unsigned int angle, int hypotenuse)
{
   double returnValue = sin(degrees_to_radian(angle));
   returnValue = returnValue * hypotenuse;
   return (float) returnValue;
}

double Skeet::degrees_to_radian(double deg)
{
   return deg * M_PI / 180.0;
}

/****************************************
 * STRIKE
 * did a bullet strike the ball?
 * tCheck each bullet
 ****************************************/
void Skeet::strike()
{
   // check to see if any of the bullets hit the pigeon
   float hit = pigeon.getRadius(); // the radius of the pigeon - used for determining if we hit the pigion
   // loop through the bullets
   for (int i = 0; i < MAX_BULLETS; i++)
   {
      // get the minimum distance between the bullet and the pigeon
      if (!bullets[i]->isDead())
        {
           // if numDistance <  hit we hit the pigeon
           if (minDistance(bullets[i], pigeon) <= hit)
           {
             incScore(); // increment score
             pigeon.kill();
             bullets[i]->kill();
           }
        }
    }
}

/*******************************************************
 * minDistance
 * function to get the mindistance between two Points
 * uses the vectors of both the pigeon and bullet
 * to get the distance between the pigeon and the bullet 
 *******************************************************/
float Skeet::minDistance(Bullet *bullet, Pigeon & pigeon)
{
   float minDist = 1000000; // start with a huge number
   float sliceP = (float) (1 / fmax(fmax(abs((int) bullet->getVelocityX()),
                                         abs((int) bullet->getVelocityY()) ),
                                    fmax(abs((int) pigeon.getVelocityX()),
                                   abs((int) pigeon.getVelocityY()))));
   for (float percent = 0; percent < 1;percent += sliceP)
      {
      float distanceSquared = (float) (pow(((bullet->getX() +
                                    bullet->getVelocityX() *
                                    percent) -
                                            (pigeon.getX() +
                                   pigeon.getVelocityX() *
                                   percent)), 2.0) +
                                       pow(((bullet->getY() +
                                    bullet->getVelocityY() *
                                    percent) -
                                  (pigeon.getY() +
                                   pigeon.getVelocityY() *
                                   percent)), 2.0));
      minDist = (float) fmin(distanceSquared, minDist);
      }
   return (float) sqrt(minDist);
}

void Skeet::onQ()
{
   cleanUp();
   exit(0); // We need to quit
}

Skeet::~Skeet()
{
   cleanUp();
}

void Skeet::cleanUp()
{
   for(int i = 0; i < MAX_BULLETS; i++)
   {
      delete bullets[i];
   }
}
