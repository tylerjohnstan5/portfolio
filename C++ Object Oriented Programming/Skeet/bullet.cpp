#include "bullet.h"

Bullet::Bullet(int x, int y)
{
   velocity_x = -2;
   velocity_y = 2;
   location.setX(x);
   location.setY(y);
   location.setCheck(true);
}

void Bullet::draw()
{
   if(!location.isDead())
   {
      Point bullet(location.getX(), location.getY());
      drawDot(bullet);
   }
}

void Bullet::advance()
{
   location.addX(velocity_x);
   location.addY(velocity_y);
}

void Bullet::kill()
{
   location.kill();
}

bool Bullet::isDead()
{
   return location.isDead();
}

void Bullet::setX(float x)
{
   location.setX(x);
}

void Bullet::setY(float y)
{
   location.setY(y);
}

void Bullet::resurrect()
{
   location.resurrect();
}

void Bullet::setVelocityX(float x)
{
   velocity_x = x/2.5;
}

void Bullet::setVelocityY(float y)
{
   velocity_y = y/2.5;
}

float Bullet::getX()
{
   return location.getX();
}

float Bullet::getY()
{
   return location.getY();
}
