#include "rifle.h"

void Rifle::draw()
{
   drawRect(location, width, height, angle);
}

Rifle::Rifle() : angleMax(180), angleMin(90), angle(120), width(50), height(5)
{
//   this->location.setX(0);
//   this->location.setY(0);
   location.setX(location.getXMax());
   location.setY(location.getYMin());
}

void Rifle::onMoveRight(int pressedFrameCount)
{
   if(angle > angleMin)
   {
      if(pressedFrameCount >= 5)
      {
         angle -= 3;
      }
      else
      {
         angle -= 2;
      }
   }

   if(angle < angleMin)
   {
      angle = angleMin;
   }
}

void Rifle::onMoveLeft(int pressedFrameCount)
{
   if(angle < angleMax)
   {
      if(pressedFrameCount >= 5)
      {
         angle += 3;
      }
      else
      {
         angle += 2;
      }
   }

   if(angle > angleMax)
   {
      angle = angleMax;
   }
}

unsigned int Rifle::getAngle()
{
   return angle;
}

int Rifle::getWidth()
{
   return width;
}
